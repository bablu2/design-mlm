import Head from "next/head";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Leftnav from "../../../../Components/Leftnav";
import api from '../../../../api/Apis'
import * as React from 'react';
import TextField from '@material-ui/core/TextField';
import DateRangePicker from '@material-ui/lab/DateRangePicker';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import Box from '@material-ui/core/Box';
import MobileDateRangePicker from '@material-ui/lab/MobileDateRangePicker';
import Moment from 'moment';
import ReactPaginate from 'react-paginate';
import Form from "../../../Components/Wallet/Form";
import FormData from "../../../Components/Wallet/FormData";
import FormDataKaieCash from "../../../Components/Wallet/FormDataKaieCash";
import { useRouter } from "next/router";
import moment from "moment"


const Commission = ({ validateauth, setshowloader, showprotectedpage }) => {
    const router = useRouter();

    const [LoginToken, setLoginToken] = useState()
    const { register, handleSubmit, errors } = useForm();
    const [value, setValue] = useState([null, null]);
    const [iscustomdate, IsCustomdate] = useState('month')

    const [offset, setOffset] = useState(0);
    const [offset1, setOffset1] = useState(0);

    const [data, setData] = useState();
    const [perPage] = useState(5);
    const [pageCount, setPageCount] = useState(0)
    const [pageCount1, setPageCount1] = useState(0)

    const [transactiondata, setTransactionData] = useState()
    const [transactiondataError, setTransactiondataError] = useState()
    const [filterdata, setfilterdata] = useState()
    const [viewtype, setViewtype] = useState(router?.query?.logtype ? router?.query?.logtype : 'Commissions')

    const [transactiondatakaire, setTransactionDatakaire] = useState()
    const [transactiondataErrorkaire, setTransactiondataErrorkaire] = useState()
    const [currentPageCommision, setcurrentPageCommision] = useState(0)
    const [currentPagekaire, setcurrentPageKaire] = useState()
    useEffect(() => {
        validateauth()

        if (router?.query?.logtype !== "kairecash") {
            if (router?.query?.logtype !== "Commissions") {
                if (Object.keys(router?.query).length > 1) {
                    router.push('/us/404/')
                }
            }


        }


        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLoginToken(token)
        {
            setshowloader(true)

            filterdata === undefined && viewtype === 'Commissions' ?

                api.getAllTranaction(token, 'month').then(res => {
                    getformatCommisiondate()

                    getformatCommisiondate(res)
                    setshowloader(false)




                })

                :
                api.getKaireCashTranaction(token, 'month').then(res => {

                    setshowloader(false)

                    getformatKaire()

                    getformatKaire(res)


                })

        }
    }, [offset, viewtype, router?.query?.logtype]);

    const onSubmit = data => {
        // setfilterdata(data)
        setshowloader(true)

        if (iscustomdate === 'custom') {
            let daterange = convert(value);
            daterange = daterange.split(" - ");
            const from_date = Moment(daterange[0]).format('YYYY-MM-DD');
            const til_date = Moment(daterange[1]).format('YYYY-MM-DD');
            data.from_date = from_date
            data.til_date = til_date
        }
        {
            viewtype === 'kairecash' ?
                api.getKaireCashTranaction(LoginToken, data?.filter_by_date, data).then(res => {
                    getformatKaire()
                    setshowloader(false)

                    getformatKaire(res)

                })
                :

                api.getAllTranaction(LoginToken, data?.filter_by_date, data).then(res => {
                    setshowloader(false)

                    getformatCommisiondate(res)


                })

        }

    }

    const convert = dateRange =>
        dateRange.map(date => new Intl.DateTimeFormat('en-US').format(new Date(date)))
            .join(" - ")
    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        setcurrentPageCommision(selectedPage)
        const offset = selectedPage * perPage;
        if (filterdata !== undefined) {
            if (viewtype === 'kairecash')
                FilterData(filterdata, offset1)
            else
                FilterData(filterdata, offset)

        }
        if (viewtype === 'kairecash')
            setOffset1(offset)
        else
            setOffset(offset)


    };

    const handlePageClick1 = (e) => {
        const selectedPage = e.selected;
        setcurrentPageKaire(selectedPage)
        const offset = selectedPage * perPage;
        if (filterdata !== undefined) {
            if (viewtype === 'kairecash')
                FilterData(filterdata, offset1)
            else
                FilterData(filterdata, offset)

        }
        if (viewtype === 'kairecash')
            setOffset1(offset)
        else
            setOffset(offset)


    };


    const FilterData = (data, offset) => {
        setfilterdata(data)

        if (iscustomdate === 'custom') {
            let daterange = convert(value);
            daterange = daterange.split(" - ");
            const from_date = Moment(daterange[0]).format('YYYY-MM-DD');
            const til_date = Moment(daterange[1]).format('YYYY-MM-DD');
            data.from_date = from_date
            data.til_date = til_date
        }

        {
            viewtype === 'kairecash' ?
                api.getKaireCashTranaction(LoginToken, data?.filter_by_date, data).then(res => {
                    getformatKaire()
                    getformatKaire(res)

                })
                :

                api.getAllTranaction(LoginToken, data?.filter_by_date, data).then(res => {
                    getformatCommisiondate()
                    getformatCommisiondate(res)

                })

        }

    }




    const getformatCommisiondate = (res) => {
        let oldDate = ''
        let test = [{ "name": "abc", "value": '12' }, { "name": "abc1", "value": '1212' }, { "name": "abc12", "value": '121' }]


        let tabledata = []
        res?.data?.transactions?.map((trans, index) => {

            tabledata.push({
                "Available_Wallet_Balance": `$${parseFloat(+trans?.available_wallet_balance).toFixed(2)}`,
                "Commission_Amount": parseFloat(+trans?.commission_balance).toFixed(2),
                "Date": moment(trans?.created_at).format(`${process.env.Date_Format}`),
                "Status": trans?.status,
                "history": [
                    { date: moment(trans?.created_at).format(`${process.env.Date_Format}`), OrderId: trans?.order, amount: trans?.commission_balance },
                ],
            })

            if (oldDate === moment(trans?.created_at).format(`${process.env.Date_Format}`)) {
                const objIndex = tabledata?.findIndex((obj => obj.Date === moment(trans?.created_at).format(`${process.env.Date_Format}`)));
                if (objIndex >= 0) {
                    tabledata[objIndex].history.push({ date: moment(trans?.created_at).format(`${process.env.Date_Format}`), OrderId: trans?.order, amount: trans?.commission_balance })
                }

            }

            {
                test?.map((testdata) => {
                    oldDate = moment(trans?.created_at).format(`${process.env.Date_Format}`)

                })
            }
        })

        let array = tabledata
        let result = Object.values(array.reduce((a, { Available_Wallet_Balance, Date, Commission_Amount, Status, history }) => {
            a[Date] = (a[Date] || { Date, Commission_Amount: 0, Available_Wallet_Balance, Status, history });
            a[Date].Commission_Amount = Number(a[Date].Commission_Amount) + Number(Commission_Amount);
            return a;
        }, {}));


        const slice = result?.slice(offset, offset + perPage)
        setPageCount(Math.ceil(result?.length / perPage))
        setTransactionData(slice)
        if (res?.data?.code === 1) {
            setTransactiondataError(res?.data?.message)
        }
        if (res?.data?.code === 0) {
            setTransactiondataError()
        }

    }



    const getformatKaire = (res) => {
        // let oldDate = ''
        // let test = [{ "name": "abc", "value": '12' }, { "name": "abc1", "value": '1212' }, { "name": "abc12", "value": '121' }]


        // let tabledata = []
        // res?.data?.transactions?.map((trans, index) => {

        //     tabledata.push({
        //         "Available_Wallet_Balance": `$${parseFloat(+trans?.available_kaire_cash).toFixed(2)}`,
        //         "Commission_Amount": parseFloat(+trans?.kaire_amount).toFixed(2),
        //         "Date": moment(trans?.created_at).format('MM/DD/YYYY'),
        //         "Status": trans?.status,
        //         "history": [
        //             { date: moment(trans?.created_at).format('MM/DD/YYYY'), OrderId: trans?.order, amount: trans?.kaire_amount },
        //         ],
        //     })

        //     if (oldDate === moment(trans?.created_at).format('MM/DD/YYYY')) {
        //         const objIndex = tabledata?.findIndex((obj => obj.Date === moment(trans?.created_at).format('MM/DD/YYYY')));
        //         if (objIndex >= 0) {
        //             tabledata[objIndex].history.push({ date: moment(trans?.created_at).format('MM/DD/YYYY'), OrderId: trans?.order, amount: trans?.kaire_amount })
        //         }

        //     }

        //     {
        //         test?.map((testdata) => {
        //             oldDate = moment(trans?.created_at).format('MM/DD/YYYY')

        //         })
        //     }
        // })

        // let array = tabledata
        // let result = Object.values(array.reduce((a, { Available_Wallet_Balance, Date, Commission_Amount, Status, history }) => {
        //     a[Date] = (a[Date] || { Date, Commission_Amount: 0, Available_Wallet_Balance, Status, history });
        //     a[Date].Commission_Amount = Number(a[Date].Commission_Amount) + Number(Commission_Amount);
        //     return a;
        // }, {}));


        // const slice = result?.slice(offset1, offset1 + perPage)
        // setPageCount1(Math.ceil(result?.length / perPage))
        // setTransactionDatakaire(slice)
        // if (res?.data?.code === 1) {
        //     setTransactiondataErrorkaire(res?.data?.message)
        // }
        // if (res?.data?.code === 0) {
        //     setTransactiondataErrorkaire()
        // }

        const slice = res?.data?.transactions?.slice(offset1, offset1 + perPage)
        setPageCount1(Math.ceil(res?.data?.transactions?.length / perPage))
        setTransactionDatakaire(slice)
        if (res?.data?.code === 0) {
            setTransactiondataErrorkaire(res?.data?.message)
        }
        if (res?.data?.code === 1) {
            setTransactiondataErrorkaire()
        }
    }
    return (<>
        <Head>
            <title>Wallet-Transactions</title>
        </Head>
        {showprotectedpage === true &&

            <div className="transactions-detail-sec mainorder-detail-sec">
                <div className="container">


                    <div className="row">
                        <Leftnav />
                        <div className="col-md-9 dashboard-main">


                            <Form value={value} setValue={setValue} iscustomdate={iscustomdate} register={register} IsCustomdate={IsCustomdate} handleSubmit={handleSubmit} onSubmit={onSubmit} />
                            <div className="groups tabs">
                                <ul className="tabs">
                                    <li
                                        className={`col-md-6 ${viewtype === 'Commissions' ? 'active' : ''}`}
                                        onClick={() => {
                                            setViewtype('Commissions')
                                        }}

                                    >
                                        <button>Commissions</button></li>
                                    <li
                                        //className="col-md-6"
                                        className={`col-md-6 ${viewtype === 'kairecash' ? 'active' : ''}`}

                                        onClick={() => {
                                            setViewtype('kairecash')
                                        }}
                                    ><button>Kaire Cash</button></li>
                                </ul>

                                {viewtype === 'Commissions' ?
                                    <div className="transactions-data-table">
                                        <FormData currentPageCommision={currentPageCommision} transactiondataError={transactiondataError} handlePageClick={handlePageClick} pageCount={pageCount} transactiondata={transactiondata} />

                                    </div>
                                    :

                                    <div className="transactions-data-table">
                                        <FormDataKaieCash currentPagekaire={currentPagekaire} transactiondataError={transactiondataErrorkaire} handlePageClick1={handlePageClick1} pageCount={pageCount1} transactiondata={transactiondatakaire} />

                                    </div>
                                }
                            </div>





                        </div>
                    </div>

                </div>
            </div>
        }
    </>)
}

export default Commission;