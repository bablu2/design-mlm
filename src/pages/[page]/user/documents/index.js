import Head from "next/head";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Leftnav from "../../../../Components/Leftnav";
import api from '../../../../api/Apis'
import { FaDownload } from "react-icons/fa";


const Documents = ({ validateauth, setshowloader, showprotectedpage }) => {
    const router = useRouter();
    const [documentdata, setDocumentData] = useState()
    // getDocuments
    useEffect(() => {
        validateauth()
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';

        api.getDocuments(token).then(res => {
            // console.log('res', res)
            setDocumentData(res?.data)

        })

    }, [])


    const downloaddata = (url1, docpath) => {
        const files = docpath.split('/')
        fetch(url1, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/pdf',
            },
        })
            .then((response) => response.blob())
            .then((blob) => {
                // Create blob link to download
                const url = window.URL.createObjectURL(
                    new Blob([blob]),
                );
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute(
                    'download',
                    `${files[files.length - 1]}`,
                );

                // Append to html link element page
                document.body.appendChild(link);

                // Start download
                link.click();

                // Clean up and remove the link
                link.parentNode.removeChild(link);
            });

    }
    return (<>
        <Head>
            <title>Documents</title>
        </Head>
        {showprotectedpage === true &&

            <div className="transactions-detail-sec mainorder-detail-sec">
                <div className="container">


                    <div className="row">
                        <Leftnav />
                        <div className="col-md-9 dashboard-main">

                            <h3 className="title">Documents</h3>
                            <div className="row">
                                <div className="col-md-9">
                                    Name
                                    </div>
                                <div className="col-md-3">
                                    Download
                                    </div>
                            </div>
                            {documentdata?.data.map((doc, index) => {
                                return (
                                    <div className="row">
                                        <div className="col-md-9">

                                            <a href={`${process.env.DOC_URL}${doc.document}`} download={`${doc.document_name}`} target="_blank" >{doc.document_name}</a>
                                        </div>

                                        <div className="col-md-3">
                                            <FaDownload onClick={() => {
                                                downloaddata(`${process.env.DOC_URL}${doc.document}`, doc.document)
                                            }} />
                                        </div>

                                    </div>
                                )

                            })
                            }

                        </div>
                    </div>

                </div>
            </div>
        }
    </>)
}

export default Documents;