import NumberFormat from 'react-number-format';
import ReactPaginate from 'react-paginate';
import moment from "moment"
import CollapsibleTable from './TableData';

const FormData = ({ currentPagekaire, transactiondata, transactiondataError, handlePageClick1, pageCount }) => {
    console.log('transactiondata', transactiondata)
    let oldDate = ''
    console.log('transactiondata kaire cash', transactiondata)
    return (
        <>

            {/* <CollapsibleTable transactiondata={transactiondata} /> */}

            <table className="commission-table">
                <thead>
                    <th>
                        Date
                            </th>
                    <th>
                        Kaire Amount
                            </th>
                    <th>
                        Status
                            </th>
                    <th>
                        Available Wallet Balance
                            </th>

                </thead>
                <tbody>
                    {transactiondataError && <tr className="title_error"><td colSpan="4" className="error-commision">{transactiondataError}</td></tr>}

                    {transactiondata?.map((tranaction, index) => {

                        if (index > 0) {
                            oldDate = tranaction?.created_at
                        }
                        return (<>
                            {oldDate !== tranaction?.created_at &&
                                <tr className="date">
                                    <td> {moment(tranaction?.created_at).format(`${process.env.Date_Format}`)}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                </tr>
                            }
                            <tr>
                                <td> </td>
                                <td>

                                    <NumberFormat
                                        value={parseFloat(+tranaction?.kaire_amount).toFixed(2)}
                                        displayType={'text'} thousandSeparator={true} prefix={'$'}
                                        renderText={value => <div> {value} </div>} />
                                </td>
                                <td>{tranaction?.status}</td>
                                <td>
                                    <NumberFormat
                                        value={parseFloat(+tranaction?.available_kaire_cash).toFixed(2)}
                                        displayType={'text'} thousandSeparator={true} prefix={'$'}
                                        renderText={value => <div> {value} </div>} />
                                </td>

                            </tr>

                            {/* {oldDate = tranaction?.created_at
                        } */}




                        </>)


                    })

                    }
                </tbody>
            </table>
            {transactiondataError === undefined &&

                <ReactPaginate
                    forcePage={currentPagekaire}

                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={pageCount}
                    marginPagesDisplayed={10}
                    pageRangeDisplayed={10}
                    onPageChange={handlePageClick1}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"} />
            }
        </>
    )
}
export default FormData;