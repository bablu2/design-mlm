

import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react';
import api from '../../../api/Apis';
import NumberFormat from 'react-number-format';
import { useForm } from 'react-hook-form';
import NProgress from 'nprogress'
import { toast } from 'react-toastify';
import BundleRefund from './vieworder/BundleRefund';
import ProductRefund from './vieworder/ProductRefund';
import RefundHistory from './refundhistory';

export default function ViewOrderDetails({ orderid, setorderid, setshowdetailsorder }) {
    const router = useRouter();
    const [data, setdata] = useState()
    const [refundHistoryData, setRefundHistoryData] = useState()
    const [Logintoken, setLogintoken] = useState()
    const [BundleData, setBundleData] = useState()

    useEffect(() => {
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLogintoken(token)
        const update_data = {
            token: token,
            order_id: orderid
        }
        api.getOrderDetailForRefund(update_data).then(res => {
            if (res?.data.code === 1) {
                setdata(res?.data?.orders)
            }
        })

        api.getRefundHistory(orderid, token).then(res => {
            if (res?.data.code === 1) {
                setRefundHistoryData(res?.data?.refund_details)
            }
        })

    }, [orderid]);
    const Backfromhere = (e) => {

        setorderid()
        setshowdetailsorder(false)
    }
    let product_qty;

    return (<>
        <Head>
            <title>Order Details</title>
        </Head>
        <div className="container">
            <h4 className="cstm-tittle">Order #{data?.public_order_id}</h4>

            <div className="row order-cstm">
                <div className="col-md-6">
                    <div className="thn-lft">
                        <h4>Billing Address</h4>
                        <div> {data?.billing_address?.first_name}</div><div>{data?.billing_address?.last_name}</div><div>
                            {data?.billing_address?.company_name}</div><div>{data?.billing_address?.state}</div><div>{data?.billing_address?.country} </div>
                    </div>
                </div>
                <div className="col-md-6">
                    <div className="thn-lft thn-rgt">
                        <h4>Shippping Address</h4>
                        <div> {data?.shipping_address?.first_name}</div><div>{data?.shipping_address?.last_name}</div><div>
                            {data?.shipping_address?.company_name}</div><div>{data?.shipping_address?.state}</div><div>{data?.shipping_address?.country} </div>
                    </div>
                </div>


            </div>
            <br /><br />
            <div className="Cart_product order-detail-sec">
                <h4 className="tittle">Order details</h4>

                <div className="orer-details-tables">
                    <table>
                        <thead><th>Product</th><th>Qty</th><th>Total</th></thead>
                        <tbody>


                            {data?.order_details.map((datas) => {
                                let is_variant;

                                try {
                                    is_variant = JSON.parse(datas?.metadata)
                                }
                                catch {
                                    is_variant = ''
                                }
                                const product_type = is_variant[0]?.fields?.product_type
                                if (product_type === 'Bundle') {
                                    api.getBundleProduct(datas?.product, Logintoken).then(res => {
                                        const datassss = res?.data?.bundle_data;
                                        if (BundleData === undefined) {
                                            setBundleData(datassss)
                                        }
                                    })
                                }
                                return (
                                    <>
                                        {product_type === 'Bundle' ?
                                            BundleData?.map((data_bundle, index) => {

                                                return <BundleRefund
                                                    BundleData={BundleData}
                                                    data_bundle={data_bundle}
                                                    product_qty={data_bundle.quantity}
                                                    is_autoship={datas?.is_autoship}

                                                />
                                            })
                                            :
                                            <ProductRefund datas={datas} product_qty={product_qty} />
                                        }
                                    </>)
                            })
                            }


                            <tr >
                                <td>Subtotal:</td>
                                <td colSpan="2">

                                    <NumberFormat value={parseFloat(data?.amount).toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <div className="details_price">{value}</div>} />
                                </td>
                            </tr>
                            {data?.discount_amount > 0 && <>
                                <tr>
                                    <td>
                                        Discount {`(${data?.coupon_name !== undefined ? (data.coupon_name) : ''})`}:
                                   </td>
                                    <td colSpan="2">
                                        <NumberFormat value={parseFloat(data?.discount_amount).toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <div className="details_price">{value}</div>} />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Total :
                        </td>
                                    <td colSpan="2">
                                        <NumberFormat value={parseFloat(data?.amount_paid).toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <div className="details_price">{value}</div>} />
                                    </td>
                                </tr>

                            </>}

                        </tbody>
                    </table>
                </div>
            </div>


            {refundHistoryData && <RefundHistory data={data} refundHistoryData={refundHistoryData} />}
            <div className="row">
                <div className="col-md-6">
                    <button className="re-order" onClick={(e) => {
                        Backfromhere()
                    }} >Back</button>
                </div>

            </div>
        </div>
    </>)
}