import React from 'react';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

import moment from "moment"
import { useRouter } from 'next/router';


function Row(props) {
    const { row } = props;
    const [open, setOpen] = React.useState(false);
    const router = useRouter()
    return (
        <React.Fragment>
            <TableRow className="main">
                <TableCell>
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                {/* <TableCell component="th" scope="row">
                    {row.name}
                </TableCell> */}
                <TableCell align="right">{row.Date}</TableCell>
                <TableCell align="right">${parseFloat(+row.Commission_Amount).toFixed(2)}</TableCell>
                <TableCell align="right">{row.Status}</TableCell>
                <TableCell align="right">{row.Available_Wallet_Balance}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={1}>
                            <Typography variant="h6" gutterBottom component="div">
                                History
              </Typography>
                            <Table size="small" aria-label="purchases">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Date</TableCell>
                                        <TableCell>Order ID</TableCell>
                                        <TableCell>Customer Name</TableCell>

                                        <TableCell align="right">Commission</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {row.history.map((historyRow) => (
                                        <TableRow key={historyRow.date}>
                                            <TableCell scope="row">
                                                {historyRow.date}
                                            </TableCell>
                                            <TableCell
                                                onClick={() => {
                                                    router.push({
                                                        pathname: `/${router.query.page}/user/commissions/`,
                                                        query: { Commission_order_id: historyRow.OrderId?.id }
                                                    });
                                                }}

                                                className="clickableicon"

                                            >#{historyRow.OrderId?.public_order_id}</TableCell>
                                            <TableCell>{historyRow.OrderId?.user?.first_name} {historyRow.OrderId?.user?.last_name}</TableCell>

                                            <TableCell align="right">${historyRow.amount}</TableCell>

                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

export default function CollapsibleTable({ transactiondata }) {

    return (
        <TableContainer component={Paper}>
            <Table aria-label="collapsible table">
                <TableHead>
                    <TableRow>
                        <TableCell />
                        <TableCell align="right">Date</TableCell>
                        <TableCell align="right">Commission Amount</TableCell>
                        <TableCell align="right">Status</TableCell>
                        <TableCell align="right">Available Wallet Balance</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {transactiondata?.map((row) => (
                        <Row key={row.name} row={row} />
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
