import NumberFormat from 'react-number-format';
import ReactPaginate from 'react-paginate';
import moment from "moment"
import CollapsibleTable from './TableData';

const FormDataKaieCash = ({ currentPageCommision, transactiondata, transactiondataError, handlePageClick, pageCount }) => {
    console.log('transactiondata', transactiondata)

    return (
        <>
            {/* <table className="commission-table">
            <thead>
                <th>
                    Date
                            </th>
                <th>
                    Commission Amount
                            </th>
                <th>
                    Status
                            </th>
                <th>
                    Available Wallet Balance
                            </th>

            </thead>
            <tbody>
                {transactiondataError && <tr className="title_error"><td colSpan="4" className="error-commision">{transactiondataError}</td></tr>}

                {transactiondata?.map((tranaction, index) => {


                    return (<>
                        <tr className="date">
                            <td> {moment(tranaction?.Date).format('MM/DD/YYYY')}</td>
                            <td></td>
                            <td></td>
                            <td></td>

                        </tr>
                        <tr>
                            <td> </td>
                            <td>

                                <NumberFormat
                                    value={parseFloat(+tranaction?.Commission_Amount).toFixed(2)}
                                    displayType={'text'} thousandSeparator={true} prefix={'$'}
                                    renderText={value => <div> {value} </div>} />
                            </td>
                            <td>{tranaction?.Status}</td>
                            <td>
                                <NumberFormat
                                value={parseFloat(+tranaction?.Available_Wallet_Balance).toFixed(2)}
                                displayType={'text'} thousandSeparator={true} prefix={'$'}
                                renderText={value => <div> {value} </div>} />
                                
                                {tranaction?.Available_Wallet_Balance}
                            </td>

                        </tr>




                    </>)


                })

                }
            </tbody>
        </table> */}


            <CollapsibleTable transactiondata={transactiondata} />
            {transactiondataError === undefined &&

                <ReactPaginate
                    forcePage={currentPageCommision}

                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={pageCount}
                    marginPagesDisplayed={10}
                    pageRangeDisplayed={10}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"} />
            }
        </>
    )
}
export default FormDataKaieCash;