import NumberFormat from 'react-number-format';
import ReactPaginate from 'react-paginate';
import moment from "moment"

const FormData = ({ setFirstDate, setLastDate, allcommissiondata, commissionData, commissionDataError, handlePageClick, pageCount, ShoworderDetails, CsvData }) => {
    let oldDate = ''
    allcommissiondata?.map((comision, index) => {
        if (index === 0) {
            setFirstDate(moment(comision?.created_at).format(`${process.env.Date_Format}`))
        }
        setLastDate(moment(comision?.created_at).format(`${process.env.Date_Format}`))

        CsvData.push({
            "Name": moment(comision?.created_at).format(`${process.env.Date_Format}`),
            "Order Info": '',
            "Product Info": '',
            "Price": '',
            "Bonusable Volume": '',
            "Commission": '',
            "Status": ''

        })
        CsvData.push({
            "Name": `${comision?.for_order?.user?.first_name}  ${comision?.for_order?.user?.last_name}`,
            "Order Info": `ORD${comision?.for_order?.id}`,
            "Product Info": '',
            "Price": '',
            "Bonusable Volume": '',
            "Commission": '',
            "Status": ''

        })
        comision?.user_commission_detail.map((comisionprd, index1) => {
            CsvData.push({
                "Name": '',
                "Order Info": '',
                "Product Info": comisionprd?.product?.name,
                "Price": `$${parseFloat(+comisionprd?.price_per_unit * +comisionprd?.product_quantity).toFixed(2)}`,
                "Bonusable Volume": `BV $${parseFloat(comisionprd?.product_bonus_value).toFixed(2)}`,
                "Commission": `$${parseFloat(+comisionprd?.commission).toFixed(2)} - ${parseFloat(comision?.commission_percentage).toFixed(2)} %`,
                "Status": `${comisionprd?.is_approved === false ?
                    'Pending' : 'Paid'
                    }`
            })
        })
    })
    return (
        <><table className="commission-table">
            <thead>
                <th>
                    Name
                            </th>
                <th>
                    Order Info
                            </th>
                <th>
                    Product Info
                            </th>
                <th>
                    Price
                            </th>
                <th>
                    Bonusable Volume
                            </th>
                <th>
                    Commission
                            </th>
                <th>
                    Status
                            </th>
            </thead>
            <tbody>
                {commissionDataError && <tr className="title_error"><td colSpan="7" className="error-commision">{commissionDataError}</td></tr>}

                {commissionData?.map((comision, index) => {


                    return (<>
                        {oldDate !== comision?.created_at &&
                            <tr className="date">
                                <td> {moment(comision?.created_at).format(`${process.env.Date_Format}`)}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        }
                        <tr>
                            <td >
                                {
                                    `${comision?.for_order?.user?.first_name}  ${comision?.for_order?.user?.last_name}`

                                }
                                <br />
                                {`(Level${comision?.commission_level} )`}


                            </td>
                            <td className="clickable" onClick={() => {
                                ShoworderDetails(comision?.for_order?.id)
                            }}>
                                {comision?.for_order?.public_order_id}
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>



                        {

                            comision?.user_commission_detail.map((comisionprd, index1) => {
                                oldDate = comision?.created_at;


                                return (
                                    <>
                                        <tr>

                                            <td></td>

                                            <td></td>
                                            <td>
                                                {comisionprd?.product?.name}
                                            </td>
                                            <td >
                                                <NumberFormat
                                                    value={parseFloat(+comisionprd?.price_per_unit * +comisionprd?.product_quantity).toFixed(2)}
                                                    displayType={'text'} thousandSeparator={true} prefix={'$'}
                                                    renderText={value => <div> {value} </div>} />

                                            </td>
                                            <td >
                                                <NumberFormat
                                                    value={parseFloat(comisionprd?.product_bonus_value).toFixed(2)}
                                                    displayType={'text'} thousandSeparator={true} prefix={'$'}
                                                    renderText={value => <div> BV {value} </div>} />
                                            </td>
                                            <td >
                                                <NumberFormat
                                                    value={parseFloat(+comisionprd?.commission).toFixed(2)}
                                                    displayType={'text'} thousandSeparator={true} prefix={'$'}
                                                    renderText={value => <div> {value} </div>} />-
                                                         <NumberFormat
                                                    value={parseFloat(comision?.commission_percentage).toFixed(2)}
                                                    displayType={'text'} thousandSeparator={true} prefix={''}
                                                    renderText={value => <div> {value} % </div>} />
                                            </td>

                                            <td >
                                                {comisionprd?.is_approved === false ?
                                                    'Pending' : 'Paid'
                                                }
                                            </td>
                                        </tr>
                                    </>
                                )
                            })

                        }




                    </>)


                })

                }
            </tbody>
        </table>
            {commissionDataError === undefined &&

                <ReactPaginate
                    previousLabel={"prev"}
                    nextLabel={"next"}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={pageCount}
                    marginPagesDisplayed={10}
                    pageRangeDisplayed={10}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"} />
            }
        </>
    )
}
export default FormData;