import React, { useEffect, useState } from 'react';
import ImageUploading from 'react-images-uploading';
import api from '../../../api/Apis';
import { ToastContainer, toast } from 'react-toastify';
import { FaTrash, FaEdit } from "react-icons/fa";

export default function ImageUpload({ setshowloader, profilepageData }) {
    const [images, setImages] = React.useState([]);
    const [logintoken, setLogintoken] = useState()

    const maxNumber = 69;

    const onChange = (imageList, addUpdateIndex) => {
        // data for submit
        // console.log(imageList, addUpdateIndex);
        setImages(imageList);
    };
    useEffect(() => {
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLogintoken(token)
        // api.getCommissionReport(token, 'week').then(res => {
        // api.GetCommissionsApproved(token, 'week').then(res => {

    }, [])

    const UpdateImageToprofile = () => {
        console.log('images111', images[0])
        // const data = { "file": images[0].file.name, "data_url": images[0]?.data_url }
        const data = images[0].file
        setshowloader(true)
        api.profileImageUpload(data, logintoken).then(res => {
            setshowloader(false)

            if (res?.data?.code === 1) {

                toast(res?.data?.message, { duration: 5, type: res?.data.message, })

            }
        })
    }


    console.log('profilepageData', profilepageData?.user_profile?.image)

    return (
        <div className="App">

            <ImageUploading
                value={images}
                onChange={onChange}
                maxNumber={maxNumber}
                dataURLKey="data_url"
            >
                {({
                    imageList,
                    onImageUpload,
                    onImageRemoveAll,
                    onImageUpdate,
                    onImageRemove,
                    isDragging,
                    dragProps,
                }) => (
                    // write your building UI
                    <div className="upload__image-wrapper">

                        {imageList.length <= 0 && <>
                            <button
                                style={isDragging ? { color: 'red' } : undefined}
                                onClick={onImageUpload}
                                {...dragProps}
                            >
                                Click or Drop here
            </button>
            &nbsp;
                            <div className="image-section">
                                <img
                                    src={`${process.env.DOC_URL}/media/${profilepageData?.user_profile?.image}`} alt="" width="100" />
                                <div className="edit-image">
                                    <FaEdit onClick={() => { setshowdetails(order_user?.order?.id) }} />

                                </div></div></>}
                        {/* <button onClick={onImageRemoveAll}>Remove all images</button> */}
                        {imageList.map((image, index) => (
                            <div key={index} className="image-item">
                                <div className="image-section">
                                    <img src={image['data_url']} alt="" width="100" />
                                    <div className="edit-image">
                                        <FaEdit onClick={onImageUpload}
                                            {...dragProps} />

                                    </div></div>
                                <div className="image-item__btn-wrapper">
                                    {/* <button onClick={() => onImageUpdate(index)}>Update</button> */}
                                    <button onClick={() => UpdateImageToprofile()}>Update</button>

                                    {/* <button onClick={() => onImageRemove(index)}>Remove</button> */}
                                    <FaTrash onClick={() => onImageRemove(index)} />
                                </div>
                            </div>
                        ))}
                    </div>
                )}
            </ImageUploading>
        </div>
    );
}