import Head from "next/head";
import { useEffect, useState } from "react";
import Leftnav from "../../../../Components/Leftnav";
import api from '../../../../api/Apis'
import React from 'react';
import { Label } from "semantic-ui-react";
import ListView from "../../../Components/Downline/ListView";
import PieCharts from "../../../Components/Dashboard/ReactD3PieChart";
import LineGraph from "../../../Components/Dashboard/LineGraph";
import Commission from "../../../Components/Dashboard/Commission";
import NumberFormat from "react-number-format";
import { useForm } from "react-hook-form";
import FilterCommissionForm from "../../../Components/Dashboard/FilterCommissionForm";
import { useRouter } from "next/router";
import Leftsection from "../../../Components/Dashboard/Leftsection";
import DashboardStyle from './DasboardStyle';

const Dashboard = ({ validateauth, setshowloader, showprotectedpage }) => {
  const [profileData, setProfileData] = useState()
  const [value, setValue] = useState([null, null]);
  const [iscustomdate, IsCustomdate] = useState('month')
  const [linedata, setLineData] = useState()
  const [newsdata, setNewsdata] = useState()
  const [documentdata, setDocumentData] = useState()

  const router = useRouter()

  useEffect(() => {
    // setshowloader(false)

    // setshowloader(true)

    // showprotectedpage === true ? setshowloader(true) : setshowloader(false)

    validateauth()


    if (Object.keys(router?.query).length > 1) {
      router.push('/us/404/')
    }
    const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
    api.getMyProfileDetails(token).then(res => {
      if (res?.data?.code === 1) {
        setProfileData(res?.data)

      }
      if (res?.data?.code === 0) {
        setProfileData(res?.data)
      }

    })
    api.getDashboardCommissions(iscustomdate, token).then(res => {

      if (res?.data?.code === 1) {
        setLineData(res?.data?.commissions_data)
      }


    })
    api.getDashboardNews(token).then(res => {
      setNewsdata(res?.data)

    })
    api.getDocuments(token).then(res => {
      // console.log('res', res)
      setDocumentData(res?.data)

    })
    if (profileData !== undefined && linedata !== undefined && newsdata !== undefined) {
      // setshowloader(false)

    }

  }, [iscustomdate]);
  const { register, handleSubmit, errors } = useForm();



  const onSubmit = data => {
    // let daterange = convert(value);
    // daterange = daterange.split(" - ");
    // const from_date = Moment(daterange[0]).format('YYYY-MM-DD');
    // const til_date = Moment(daterange[1]).format('YYYY-MM-DD');
    // data.from_date = from_date
    // data.til_date = til_date

    // api.getCommissionsFilter(data, LoginToken).then(res => {
    //     const slice = res?.data?.commissions_data?.slice(offset, offset + perPage)
    //     setPageCount(Math.ceil(res?.data?.commissions_data?.length / perPage))
    //     setcommissionData(slice)
    // })


  }
  const convert = dateRange =>
    new Intl.DateTimeFormat('en-US').format(new Date(dateRange))

  return (<>
    <Head>
      <title>Dashboard</title>
    </Head>
    <DashboardStyle>
      {showprotectedpage === true &&
        <div className="mainorder-detail-sec dashboad-deails Dashboard_bg">
          <div className="container">
            <div className="row">
              <Leftnav />
              <div className="col-md-9 dashboard-main">
                <h3 className="title">Dashboard</h3>
                <div className="row">
                  <Leftsection
                    profileData={profileData}
                    newsdata={newsdata}
                    documentdata={documentdata}
                  />
                  <div className="col-md-9 statis_datas">

                    <div className="top-head-commission">
                      <div className="left-commison-sec">
                        <span>Total sales :  </span><NumberFormat
                          value={parseFloat(profileData?.total_sales).toFixed(2)}
                          displayType={'text'} thousandSeparator={true} prefix={'$'}
                          renderText={value => <div> {value} </div>} />
                      </div>
                      <div className="right-commison-sec">
                        Rank as per sale : {profileData?.rank_as_per_sales}{
                          +profileData?.rank_as_per_sales === 11 || +profileData?.rank_as_per_sales === 12 || +profileData?.rank_as_per_sales === 13 ? 'th' :

                            +profileData?.rank_as_per_sales % 10 === 1 ? 'st' :
                              +profileData?.rank_as_per_sales % 10 === 2 ? 'nd' :
                                +profileData?.rank_as_per_sales % 10 === 3 ? 'rd' : profileData?.rank_as_per_sales ?
                                  'th' : ''


                        }
                      </div>
                    </div>
                    <div className="main-top">
                      <FilterCommissionForm
                        value={value}
                        setValue={setValue}
                        iscustomdate={iscustomdate}
                        register={register}
                        IsCustomdate={IsCustomdate}
                        handleSubmit={handleSubmit}
                        onSubmit={onSubmit} />
                      <div className="chart-main">

                        <div className="chart-PieCharts">
                          {iscustomdate &&
                            <LineGraph linedata={linedata} filter={iscustomdate} />
                          }
                        </div>
                        {/* <div className="chart-LineGraph">

                      <PieCharts />
                    </div> */}
                      </div>

                    </div>


                    <div className="commsion-weekly-dashboard">
                      <Commission />
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      }
    </DashboardStyle>
  </>)
}
export default Dashboard;