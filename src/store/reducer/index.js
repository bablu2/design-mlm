import changeNumber from './changeNumber'
import loginToSite from './loginToSite'

import { combineReducers } from 'redux'
const rootReducers = combineReducers({
    changeNumber,
    loginToSite
})
export default rootReducers;