import NumberFormat from 'react-number-format';
import { FaTrash, FaEdit } from "react-icons/fa";
import api from '../../../api/Apis';
import { useEffect, useState } from 'react';
const AutoshipProductDetails = ({ datas, Add, Sub, data, deleteautoship, Logintoken }) => {
    return (
        <>
            <div className="col-md-4">
                <div className="cart-product-details">
                    {datas?.Product_name} X {datas?.quantity}
                </div>
            </div>
            <div className="col-md-4 qty-sec" >
                <div className="main-qty-sec">
                    <div className="box">
                        <div id="qty">
                            <button type="button"
                                data-qty={datas?.quantity}
                                data-product_id={datas?.product_id}
                                data-variant_id={null}
                                className="sub"
                                onClick={(e) => { Sub(e) }}>-</button>
                            <input
                                name={`${datas?.product?.id},${null}`}
                                type="text"
                                value={datas?.quantity}
                                readOnly
                            />
                            <button type="button"
                                data-qty={datas?.quantity}
                                data-product_id={datas?.product_id}
                                data-variant_id={null}
                                data-max_qty={+datas?.total_quantity}
                                className="add"
                                value={datas?.quantity} onClick={(e) => { Add(e) }}>+</button>
                        </div>
                    </div>
                    <div className="delete">
                        <FaTrash onClick={() => { deleteautoship(datas?.product_id, null) }} />
                    </div>
                </div>

            </div>

            <div className="col-md-4">
                <div className="cart-product-details">
                    <NumberFormat value={parseFloat(+datas?.cost * datas?.quantity).toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <div>{value}</div>} />
                </div>
            </div>
        </>
    )
}
export default AutoshipProductDetails;