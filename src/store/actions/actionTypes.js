import api from "../../api/Apis";

export const increment = () => {
    return { type: "INCREMENT" }
}
export const decrement = () => {
    return { type: "DECREMENT" }
}
// export const loggin = (data) => {
//     return { type: "LOGIN", data: data }
// }
const INCREMENT_COUNTER = 'INCREMENT_COUNTER';

export function loggin(data) {
    return {
        type: "LOGIN", data: data
    };
}

export function incrementAsync(data) {
    return dispatch => api.loginUser(data).then((res) => {
        dispatch(loggin(res.data));
    })
}