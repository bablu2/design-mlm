import { useForm } from "react-hook-form";
import api from '../../../api/Apis';
import { ToastContainer, toast } from 'react-toastify';
import { useState } from "react";

const ProfileEdit = ({ profilepageData, LoginToken, setEditDetails, setreloadData }) => {

    const { register, handleSubmit, watch, errors, getValues } = useForm();
    const [sameforpublic, setSameForPublic] = useState(true)
    // for reset password
    const onSubmit = data => {
        api.profileUpdate(data, LoginToken).then(res => {
            setEditDetails(false)
            setreloadData(true)
            if (res?.data?.code === 1) {

                toast(res?.data?.message, { duration: 5, type: "success", })

            }
            if (res?.data?.code === 0) {

                toast(res?.data?.message, { duration: 5, type: "error", })

            }
        })
    }
    return (
        <div className="update-details">

            <h4 className="update-profile">Update </h4>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="row">
                    <div className="col-md-12">
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1" className="form-label"> First Name</label>
                            <input type="text"
                                defaultValue={profilepageData?.first_name}
                                className="form-control" name="first_name" id="exampleInputPassword2" ref={register({ required: true })} />
                            {errors.first_name && <span>This field is required</span>}
                        </div>

                    </div>
                    <div className="col-md-12">
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1" className="form-label">Last Name</label>
                            <input type="text"
                                defaultValue={profilepageData?.last_name}

                                className="form-control" name="last_name" id="exampleInputPassword1"
                                ref={register({
                                    required: "This field is required",


                                })}
                            />
                            {errors.last_name && <span>{errors.last_name.message}</span>}
                        </div>

                    </div>



                    <div className="col-md-12">
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1" className="form-label">Phone Number</label>
                            <input type="text"
                                defaultValue={profilepageData?.userdetails[0]?.phone_number}


                                className="form-control" name="phone_number" id="exampleInputPassword1"
                                ref={
                                    register({
                                        required: "This field is required",
                                        pattern: {
                                            value: /^[0-9]/,
                                            message: "Enter only number"
                                        },
                                        minLength: {
                                            value: 5,
                                            message: "Enter minimum 5 digit"
                                        },
                                        maxLength: {
                                            value: 10,
                                            message: "Phone number not longer then 10 digit"
                                        },
                                    })}
                            />
                            {errors.phone_number && <span>{errors.phone_number.message}</span>}
                        </div>

                    </div>














                    {sameforpublic === false && <>

                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1" className="form-label"> Public First Name</label>
                                <input type="text"
                                    defaultValue={
                                        profilepageData?.userdetails[0]?.public_first_name ? profilepageData?.userdetails[0]?.public_first_name : profilepageData?.first_name

                                    }
                                    className="form-control" name="public_first_name" id="exampleInputPassword2" ref={register({ required: true })} />
                                {errors.public_first_name && <span>This field is required</span>}
                            </div>

                        </div>
                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1" className="form-label">Public Last Name</label>
                                <input type="text"
                                    defaultValue={profilepageData?.userdetails[0]?.public_last_name
                                        ?
                                        profilepageData?.userdetails[0]?.public_last_name
                                        :
                                        profilepageData?.last_name
                                    }

                                    className="form-control" name="public_last_name" id="exampleInputPassword1"
                                    ref={register({
                                        required: "This field is required",


                                    })}
                                />
                                {errors.public_last_name && <span>{errors.public_last_name.message}</span>}
                            </div>

                        </div>


                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor="exampleInputAge" className="form-label">Public  Email</label>
                                <input type="text" className="form-control" name="public_email" id="public_email"
                                    defaultValue={profilepageData?.userdetails[0]?.public_email
                                        ?
                                        profilepageData?.userdetails[0]?.public_email
                                        :
                                        profilepageData?.email
                                    }
                                    aria-describedby="emailnoHelp" ref={register({
                                        required: "This field is required",
                                        pattern: {
                                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                            message: "Invalid email address"
                                        }
                                    })} />
                                {errors.public_email && <span>{errors.public_email.message}</span>}
                                <div className="show-email-check">
                                    <div className="lables">Visible for public</div>
                                    <div className="content"><input
                                        defaultChecked={profilepageData?.userdetails[0]?.show_email}
                                        type="checkbox" className="form-check-input" id="Terms-Condition" name="show_email" ref={register({ required: false })} /></div>

                                </div>

                            </div>

                        </div>






                        <div className="col-md-12">
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1" className="form-label">Public Phone Number</label>
                                <input type="text"
                                    defaultValue={profilepageData?.userdetails[0]?.public_phone_number
                                        ?
                                        profilepageData?.userdetails[0]?.public_phone_number
                                        :
                                        profilepageData?.userdetails[0]?.phone_number
                                    }


                                    className="form-control" name="public_phone_number" id="exampleInputPassword1"
                                    ref={
                                        register({
                                            required: "This field is required",
                                            pattern: {
                                                value: /^[0-9]/,
                                                message: "Enter only number"
                                            },
                                            minLength: {
                                                value: 5,
                                                message: "Enter minimum 5 digit"
                                            },
                                            maxLength: {
                                                value: 10,
                                                message: "Phone number not longer then 10 digit"
                                            },
                                        })}
                                />
                                <div className="show-phone-check">
                                    <div className="lables">Visible for public</div>
                                    <div className="content"><input
                                        defaultChecked={profilepageData?.userdetails[0]?.show_phone_number}
                                        type="checkbox" className="form-check-input" name="show_phone_number" ref={register({ required: false })} /></div>

                                </div>
                                {errors.public_phone_number && <span>{errors.public_phone_number.message}</span>}


                            </div>


                        </div>



                    </>
                    }
                    <div className="col-md-12">
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1" className="form-label">Referral Code</label>
                            <input type="text"

                                defaultValue={profilepageData?.userdetails[0]?.referral_code}

                                className="form-control" name="referral_code" id="referral_code" ref={register({ required: true })} />
                            {errors.referral_code && <span>This field is required</span>}
                        </div>

                    </div>


                    <div className="col-md-12">
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1" className="form-label">Same for public</label>
                            <input type="checkbox"

                                defaultChecked={sameforpublic}
                                onChange={(e) => {
                                    setSameForPublic(e.target.checked)
                                }}
                                className="form-control" />
                        </div>

                    </div>
                </div>


                <div className="row">
                    <button type="submit" className="btn btn-primary">Update Details</button>
                </div>
            </form>
        </div>

    )
}
export default ProfileEdit;