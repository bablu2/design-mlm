
import NextNProgress from "nextjs-progressbar";
import React from "react";
import Image from 'next/image'

const Footer = ({ showloader }) => {
    return (
        <>
            <div className="fotter_section">
                <div className="container_footer">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="fotter-logo">
                                <img src="/images/main-logo.png" />
                            </div>
                            <div className="fotter-list">
                                <ul>
                                    <li><a href="#">Return Policy</a></li>
                                    <li><a href="#">Teams & Conditions</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                </ul>
                            </div>
                            <div className="fotter-listdetail">
                                <ul>
                                    <li>
                                        <a href="#">North America Toll Free Phone & Fax
                                          <span className="loc1">1-877-603-1710 (Toll-Free)</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">New Zealand Toll Free Phone & Fax
                                           <span className="loc1"> 0800-451353</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">Australia Toll Free Phone & Fax
                                         <span className="loc1">1-800-737-643</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {showloader === true &&
                <div className="loader-main">
                    <Image
                        src="/loading-96.gif"
                        width={200}
                        height={200}
                        alt="logo"
                    />
                </div>
            }
        </>);
}
export default Footer