import Head from 'next/head'
import Link from 'next/link';
import { useRouter } from 'next/router';
import styles from '../../../../styles/Home.module.css'
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { useEffect, useRef, useState } from 'react';
import api from '../../../api/Apis';
import Slider from "react-slick";
import { AiOutlineLeft, AiOutlineRight } from "react-icons/ai";
import { IconButton } from '@material-ui/core';

const Home = ({ icon, homePageData }) => {
    const [productData, setProductData] = useState([]);
    const route = useRouter();
    const store = route?.query?.page || "us";

    useEffect(() => {
        getAllProduct(store);
    }, [store])

    const getAllProduct = async (store) => {
        await api.getAllProduct(store).then((res) => {
            setProductData(res?.data?.products);
        }).catch(() => {
        })
    }
    const sliders = useRef();
    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };
    function next() {
        sliders?.current?.slickNext();
    }
    function previous() {
        sliders?.current?.slickPrev();
    }
    return (
        <>
            <div className="home-page" >
                <div className="welcome-section">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-3">
                                <div className="text-welcome">
                                    <div className="welcome-text">
                                        <h4>Welcome</h4>
                                        <p>Join us on a path to a better you. Lose weight, transform your health, and live the life you have always dreamed of*. Pioneering the Antioxidant Revolution with our legendary Maritime Prime™ Pycnogenol®, Kaire has set a standard in health supplements. Live Better, Longer!</p>
                                    </div>
                                    <h2>Live Better,<br />Longer</h2>
                                </div>
                            </div>
                            <div className="col-md-9">
                                <div className="image-welcome">
                                    <img src="images/img1.jpg" />
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="par-text">
                                    <p>Join us on a path to a better you. Lose weight, transform your health, and live the life you have always dreamed of*. Pioneering the Antioxidant Revolution with our legendary Maritime Prime™ Pycnogenol®, Kaire has set a standard in health supplements. Live Better, Longer! Join us on a path to a better you. Lose weight, transform your health, and live the life you have always dreamed of*. Pioneering the Antioxidant Revolution with our legendary Maritime Prime™ Pycnogenol®, Kaire has set a standard in health supplements. Live Better, Longer!</p>
                                    <p>Join us on a path to a better you. Lose weight, transform your health, and live the life you have always dreamed of*. Pioneering the Antioxidant Revolution with our legendary Maritime Prime™ Pycnogenol®, Kaire has set a standard in health supplements. Live Better, Longer!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="welcome-section pycn-section">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-8">
                                <div className="image-welcome">
                                    <img src="images/img2.jpg" />
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="text-welcome">
                                    <div className="welcome-text maritime-text">
                                        <h4>Maritime Prime Pycnogenol</h4>
                                        <p>It has been said, “Not all things are created equal.”  This is quite true when it comes to Kaire’s Maritime Prime - it is simply the finest, most effective Pyncnogenol formulation in the world.</p>
                                        <div className="product-main1">
                                            <img src="images/product-main.png" />
                                        </div>
                                        <div className="lean-main">
                                            <a href="#" className="btn btn-learn">Lean More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="slider-main">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="slider-main">
                                    <div className="carousel slide media-carousel" id="media">
                                        <div className="carousel-inner">
                                            {/* <Carousel
                                                additionalTransfrom={0}
                                                arrows
                                                autoPlaySpeed={3000}
                                                centerMode={false}
                                                className=""
                                                containerClass="container-with-dots"
                                                dotListClass=""
                                                draggable
                                                focusOnSelect={false}
                                                infinite
                                                itemClass=""
                                                keyBoardControl
                                                minimumTouchDrag={80}
                                                renderButtonGroupOutside={false}
                                                renderDotsOutside={false}
                                                responsive={{
                                                    desktop: {
                                                        breakpoint: {
                                                            max: 3000,
                                                            min: 1024
                                                        },
                                                        items: 4,
                                                        partialVisibilityGutter: 40
                                                    },
                                                    mobile: {
                                                        breakpoint: {
                                                            max: 464,
                                                            min: 0
                                                        },
                                                        items: 1,
                                                        partialVisibilityGutter: 30
                                                    },
                                                    tablet: {
                                                        breakpoint: {
                                                            max: 1024,
                                                            min: 464
                                                        },
                                                        items: 2,
                                                        partialVisibilityGutter: 30
                                                    }
                                                }}
                                                showDots={false}
                                                sliderClass=""
                                                slidesToSlide={1}
                                                swipeable

                                                customLeftArrow={
                                                    <a data-slide="prev" href="#media" className="left carousel-control">
                                                        <i className="fa fa-angle-left" aria-hidden="true">
                                                        </i>
                                                    </a>
                                                }
                                                customRightArrow={
                                                    <a data-slide="next" href="#media" className="right carousel-control">
                                                        <i className="fa fa-angle-right" aria-hidden="true">
                                                        </i>
                                                    </a>
                                                }
                                            >
                                                {productData?.map((values, index) => {
                                                    let names = values?.name.replaceAll('25%', '')
                                                    let catname1 = names.replaceAll('\t', '')
                                                    let catname = catname1.replaceAll(' ', '_')

                                                    return (
                                                        <div className="product-main" key={index + 1}>
                                                            <Link href={`/${store}/product/${catname}/${values?.id}`}>
                                                                <a className="thumbnail">
                                                                    <img alt="" src={`${process.env.API_URL}/${values?.product_images[0]?.image}`} />
                                                                    <h5>{values?.name}</h5>
                                                                </a>
                                                            </Link>
                                                            <a href="" className="btn btn-larn" >Learn More</a>
                                                           
                                                        </div>
                                                    )
                                                })}
                                            </Carousel> */}
                                            <div className="silck-slide-home">
                                                <IconButton className="button_pre" onClick={previous}><AiOutlineLeft /></IconButton>
                                                <Slider ref={sliders} {...settings}>
                                                    {productData?.map((values, index) => {
                                                        let names = values?.name.replaceAll('25%', '')
                                                        let catname1 = names.replaceAll('\t', '')
                                                        let catname = catname1.replaceAll(' ', '_')
                                                        return (
                                                            <div className="product-main" key={index + 1}>
                                                                <Link href={`/${store}/product/${catname}/${values?.id}`}>
                                                                    <a className="thumbnail">
                                                                        <img alt="" src={`${process.env.API_URL}/${values?.product_images[0]?.image}`} />
                                                                        <h5>{values?.name}</h5>
                                                                    </a>
                                                                </Link>
                                                                <a href="" className="btn btn-larn" >Learn More</a>
                                                            </div>
                                                        )
                                                    })}
                                                </Slider>
                                                <IconButton className="button_next" onClick={next}><AiOutlineRight /></IconButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="btn-shop-all">
                                    <Link className="nav-link" exact href={`/${store}/allProduct`}>
                                        <a className="btn btn-all">Shop All Product</a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="welcome-section gauratee-section">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-2">
                                <div className="text-welcome">
                                    <div className="welcome-text">
                                        <h4>365 Day<br />
                                            Guarantee
                                        </h4>
                                        <p>No Matter What The Reason, You Have 365 Days From The Date of Purchase to Return The Product … No Questions Asked!</p>
                                        <p>All you have to do is Contact Customer Support telling them that you wish to return your product… send us the original containers, and pay the postage… It Is as Easy As That!</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-10">
                                <div className="image-welcome">
                                    <img src="images/img3.jpg" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="welcome-section gauratee-section">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="question-text">
                                    <h1> No Questions Asked</h1>
                                    <div className="stay-touch-part">
                                        <h5>Stay in Touch</h5>
                                    </div>
                                    <div className="form-subscribe">
                                        <form className="form-inline">
                                            <div className="form-group">
                                                <label className="col-form-label">Subscribe</label>
                                                <input type="text" readOnly className="form-control-plaintext" id="staticEmail" value="*Your email address " />
                                            </div>
                                            <button type="submit" className="btn btn-primary">Subscribe</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </>
    )
}
export default Home