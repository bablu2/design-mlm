const withSass = require('@zeit/next-sass');
const withImages = require('next-images');
const withLess = require('@zeit/next-less')
const withCSS = require('@zeit/next-css')

const nextTranslate = require('next-translate');

let baseurl = 'https://commandconcept-qa.csdevhub.com/api/v1/';
module.exports = withCSS(withLess(withImages(withSass({
  env: {
    API_URL: 'https://commandconcept-qa.csdevhub.com/',
    DOC_URL: 'https://commandconcept-qa.csdevhub.com',
    API_BASE_URL: 'https://commandconcept-qa.csdevhub.com/api/v1/',
    Date_Format: 'MM-DD-YYYY',


    sinUpApi: baseurl + 'signup/',
    loginApi: baseurl + 'login/',
    logoutApi: baseurl + 'logout/',

    getBanners: baseurl + 'store_banner/',
    getHomepageContent: baseurl + 'get_homepage_content/',

    getProductByCategories: baseurl + 'get_products_by_category/',
    multiProductAddToCart: baseurl + 'add_to_cart_multiple/',
    getProductByProductid: baseurl + 'get_product_details/',
    getAllProduct: baseurl + 'get_all_products/',
    addToCart: baseurl + 'add_to_cart/',
    getAllCartProduct: baseurl + 'get_cart_details',
    deleteProductFromAddToCart: baseurl + 'delete_cart_product/',
    updateProductQty: baseurl + 'update_product_qty/',
    getProfilePageData: baseurl + 'profile_details/',
    changeUserPassword: baseurl + 'change_password/',
    addToWishlist: baseurl + 'add_to_wishlist/',
    getAllWishListProduct: baseurl + 'get_wishlist_details/',
    deleteProductFromwishlist: baseurl + 'delete_wishlist_product/',
    manageAddress: baseurl + 'manage_address/',
    getProductByVariantid: baseurl + 'get_variant_details/',
    createOrder: baseurl + 'create_order/',
    verifyCopan: baseurl + 'apply_coupon/',
    getAllCategory: baseurl + 'get_all_categories/',
    getproductsbycategory: baseurl + 'get_products_by_category/',
    updateCart: baseurl + 'update_cart/',
    getUserOrder: baseurl + 'order_history/',
    getAddressDetails: baseurl + 'get_address_details/',
    GetOrderDetail: baseurl + 'get_order_details/',
    deleteAddress: baseurl + 'delete_address/',
    reorderProducts: baseurl + 're_order/',
    getAddressList: baseurl + 'get_addresses/',
    autoshipOrderHistory: baseurl + 'autoship_order_history/',
    autoshipOrderById: baseurl + 'get_autoship_order_details/',
    autoshipUpdate: baseurl + 'autoship_update/',
    autoshipDelete: baseurl + 'autoship_delete/',
    autoshipSkip: baseurl + 'autoship_skip/',
    updateAddress: baseurl + 'update_address/',
    getUserNotifications: baseurl + 'get_user_notifications/',
    autoshipProductDelete: baseurl + 'autoship_product_delete/',
    autoshipProductUpateproduct: baseurl + 'autoship_order_add_product/',
    addressUpdate: baseurl + 'autoship_address_update/',
    resetPassword: baseurl + 'api/password_reset/',
    cancelOrder: baseurl + 'cancle_order/',
    refundOrder: baseurl + 'refund_order/',
    getDownlineUsers: baseurl + 'get_downline_users/',
    getRefundHistory: baseurl + 'get_refund_history',
    getBundleProduct: baseurl + 'get_bundle_details/',
    redeemKaireCash: baseurl + 'redeem_kaire_cash/',
    getCommissionReport: baseurl + 'get_commissions_report/',
    getCommissionsFilter: baseurl + 'get_commissions_filter/',

    getRefundReport: baseurl + 'get_clawbacks/',
    getRefundReportFilter: baseurl + 'get_clawbacks_filter/',

    getMyProfileDetails: baseurl + 'get_my_profile_details/',
    getDashboardCommissions: baseurl + 'get_dashboard_commissions/',
    GetCommissionsApproved: baseurl + 'get_commissions_approved/',

    LoginCheck: baseurl + 'login_check/',

    getOrderDetailForRefund: baseurl + 'get_new_order_details/',
    profileUpdate: baseurl + 'profile_update/',
    getAllTranaction: baseurl + 'get_commission_transactions/',
    getNewDownlineUsers: baseurl + 'get_new_downline_users/',
    getDownlineUserAddress: baseurl + 'get_downline_user_addresses/',

    getKaireCashTranaction: baseurl + 'get_kaire_cash_transactions/',
    getDashboardNews: baseurl + 'get_dashboard_news/',
    profileImageUpload: baseurl + 'profile_image_upload/',
    getDocuments: baseurl + 'get_documents/',


    JWT_KEY: 'gdfghfgh-fgfdgfdgfdhdsasafraeeyjyukktd-dfghdfhfgdhrdt',
    CSRF_SECRET: "screetenvvalue",
  },

  webpack: function (config) {
    config.module.rules.push({
      test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,

      use: {
        loader: 'url-loader',
        options: {
          limit: 100000,
          name: '[name].[ext]'
        }
      }
    },
      {
        test: /\.svg$/,
        issuer: {
          test: /\.(js|ts)?$/,
        },
        use: ['@svgr/webpack'],
      }

    )
    return config
  },

  // webpack(config) {
  //   config.module.rules.push({
  //     test: /\.svg$/,
  //     issuer: {
  //       test: /\.(js|ts)x?$/,
  //     },
  //     use: ['@svgr/webpack'],
  //   });

  //   return config;
  // },
  // i18n: {
  //   locales: ['en-US', 'fr', 'nl-NL'],
  //   defaultLocale: 'en-US',
  // },


  // ...nextTranslate(),
  // exportTrailingSlash: true,
  trailingSlash: true,

  exportPathMap: (defaultPathMap) => {



    return {
      '/': { page: '/' },

      '/us/allProduct/': { page: '/[page]/allProduct' },

      '/us/user/dashboard': { page: '/[page]/user/dashboard' },
      '/us/user/wallet/': { page: '/[page]/user/wallet' },
      '/us/user/order/create/': { page: '/[page]/user/order/create' },

      '/us/user/downline': { page: '/[page]/user/downline' },
      '/us/user/commissions': { page: '/[page]/user/commissions' },
      '/us/user/refundreport': { page: '/[page]/user/refundreport' },
      '/us/user/commission-report': { page: '/[page]/user/commission-report' },
      '/us/user/profile': { page: '/[page]/user/profile' },
      '/us/user/order': { page: '/[page]/user/order' },
      '/us/user/autoshiporder': { page: '/[page]/user/autoshiporder' },
      '/us/user/address': { page: '/[page]/user/address' },
      '/us/user/documents': { page: '/[page]/user/documents' },

      '/us/cart/viewCart/': { page: '/[page]/cart/viewCart' },
      '/us/wishlist/wishlist/': { page: '/[page]/wishlist/wishlist' },
      '/us/reset-password/': { page: '/[page]/reset-password' },
      '/us/signup/': { page: '/[page]/signup' },
      '/us/login/': { page: '/[page]/login' },
      '/us/checkout/addressList/': { page: '/[page]/checkout/addressList' },











    }
  }
}))));


