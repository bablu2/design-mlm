import styled from 'styled-components';

const ProductPageDesign = styled.div`
&
//<---------------------------------------------------------------first section css
.product-section-banner {
    background: url(/images/product-banner.png);
    background-size: 100%;
    height: 512px;
    background-repeat: no-repeat;
}

//<---------------------------------------------------------------second section css
.Images_product {
    border: 1px solid #00f
}

h6.dollar-text {
    font-size: 20px;
    color: #ebebeb;
}
.product-slider-text h5 {
    font-size:20px;
    margin: 0 0 10px;
}
.supplement-fact {
    background: #9e9e9e;
    width: 142px;
    text-align: center;
    color: #fff !important;
    border-radius: 25px;
    padding: 0;
    height: 27px;
    margin: 20px 0 10px;
    display: block;
}
.review-section-now ul {
    display: flex;
}
.supplement-fact a {
    padding: 2px;
    color: #fff;
}
.review-section-now {
    margin: 10px 0;
}
.review-section-now ul li {
    display: inline-block;
    padding-right: 20px;
}
.review-section-now ul li a{
   color:#000;
}
.review-section-now {
    margin: 15px 0;
}
.cart-add {
    margin:35px 0;
    float: left;
    width: 100%
}
.cart-add button {
    background: rgb(0, 53, 106);
    color: rgb(255, 255, 255);
    border-radius: 25px;
    width: 100%;
    font-size: 18px;
    height: 39px;
}
.frequestion-section h3 {
    font-size: 30px;
    font-weight: 600;
    position: relative;
}

.product-checkout ul {
    display: flex;
    margin: 40px 0 0;
}
.product-checkout li {
    display: inline;
    text-align: center;
    width: 30%;
}
.product-firxt img {
    width: 150px;
    height: 150px;
    object-fit: contain;
}
.product_1 h5 {
    font-size: 16px;
    padding: 10px 0 0;
    font-weight: 600;
    margin: 0 0 3px;
}
.product_1 h6 {
    margin: 0;
    padding: 0 0 6px;
    font-size: 15px;
}
.total_selected h4 {
    font-size: 26px;
    font-weight: 600;
    margin: 20px 0 0;
}
.total_selected .zero {
    font-size: 40px;
    font-weight: 600;
    text-align: center;
    width: 100%;
    margin: 0;
    padding: 0;
}

a.btn.btn-total-cart {
    background: #969696;
    margin: 35px 40px;
    border-radius: 25px;
    color: #fff;
    width: 170px;
    font-size: 20px;
    line-height: 31px;
}
.study-now {
    background: url(/images/b1.jpg);
    background-repeat: no-repeat;
    background-size: 48%;
    background-position: center;
    height: 336px;
    padding: 30px;
    transform: translateY(90%);
}
.study-now h6 {
    text-align: center;
    font-weight: 600;
    font-size: 15px;
    width: 220px;
    margin: 0 auto;
    line-height: 19px;
}
.study-now p {
    text-align: center;
    font-weight: 500;
    font-size: 14px;
    width: 220px;
    margin: 0 auto;
    line-height: 19px;
}
.study-now .lean-main {
    text-align: center;
    margin-top: 50px;
    margin-bottom: 20px;
}
.frequestion-section {
    margin: 40px 0;
    display: block;
    position: relative;
    float: left;
    width: 100%;
}

.main-qty-sec .box:last-child {
    display: flex;
    align-items: center;
    margin: 40px 0 0;
}

//<---------------------------------------------------------------third section css
.health_supplement_section {
    background: #fff;
    margin: 0;
    text-align: center;
    padding: 60px 0;
	position:relative;
}
.health_supplement_section h3 {
    font-size: 24px;
}
.health_supplement_section p {
    font-size: 14px;
    width: 970px;
    margin: 0 auto;
    line-height: 25px;
    margin-bottom: 70px;
}
.health_supplement_section::after {
    top: 0;
    position: absolute;
    content: "";
    border-top: 1px solid #ddd;
    width: 87%;
    left: 0;
    right: 0;
    margin: 0 auto;
}
.box-text {
    background: #00356a;
    display: flex;
    padding: 30px;
}
.karie-pic-now img {
    width: 100%;
}
.box-left h4 {
    color: #fff;
    font-size: 24px;
    text-align: left;
    width: 309px;
    line-height: 32px;
    margin: 0;
}
// .box-right img {
//     width: 158px;
//     position: absolute;
//     top: 59%;
//     right: 72px;
// }
.lisy-ul {
    background: #00356a;
}
.lisy-ul ul li {
    color: #fff;
    text-align: left;
}

.lisy-ul {
    background: #00356a;
    padding: 20px 50px;
    display: flex;
}
.lisy-ul ul li {
    padding-bottom: 10px;
}
.lisy-ul ul {
    width: 50%;
}
.lisy-ul ul:first-child {
    margin-right: 76px;
}
.image_secton_ {
    height: 718px;
}
.image_secton_ .gar-img {
    position: absolute;
    bottom: 0;
}
.image_secton_ .gar-img img {
    margin-bottom: 0!important;
}
.box-text {
    background: #00356a;
    display: flex;
    padding: 15px 30px 25px;
    min-height: 250px;
}
.row.both-text {
    margin-bottom:12px;
}
.lisy-ul ul li:last-child {
    padding-bottom: 0;
}
.lisy-ul ul {
    margin-bottom: 0;
}
.karie-pic-now h3 {
    background: #00356a;
    padding: 30px 0;
    color: #fff;
    font-size: 42px;
}

span.green-text {
    color: #a9cb36;
}
.health_supplement_section .gar-img img {
    width: 100%;
    margin: 10px 0;
}
.health_supplement_section .box-right img {
    width: 100%;
}
.health_supplement_section .box-right {
    position: absolute;
    width: 100%;
    max-width: 150px;
    left: calc(100% - 214px);
    bottom: 0;
}


.box-text2 h5 {
    font-size: 24px;
    font-weight: 600;
    width: 478px;
    line-height: 36px;
    text-align: center;
    margin: 0 auto;
    padding-top: 18px;
}

.blue-bg h2 {
    padding: 30px 0;
    color: #fff;
    font-size: 42px;
}
.blue-bg h5 {
    color: #fff;
    font-size: 24px;
    width: 505px;
    margin: 0 auto;
    font-weight: 600;
    line-height: 36px;
}
.blue-bg {
    background: url(/images/blue-bg.png);
    height: 718px;
}
.logo-360 {
    position: absolute;
    bottom: 52px;
    left: 0;
    right: 0;
}

//<---------------------------------------------------------------fourth section css
.review-customer {
    padding: 40px 0;
}
.review-customer h5 {
    font-size: 21px;
}.review-customer h2 {
    font-size: 45px;
    font-weight: 600;
}
.review-customer h2 {
    font-size: 45px;
    font-weight: 600;
    margin: 0;
}
.customer-review-now {
	 background: url(/images/custom-review.png);
}
.name-review ul li {
    display: inline;
    padding: 10px;
}
.name-review ul {
    margin: 17px 0;
}
.row.client-review {
    border-top: 1px solid #d0c9c9;
    padding: 20px 0;
}
.profile-1 h6 {
    font-weight: 600;
}
.comment-stars i {
    font-size: 20px;
}
.btn-pre-view {
    margin: 30px 0 205px;
    text-align: center;
}
a.prev-btn-now {
    background: #00356a;
    border-radius: 25px;
    padding: 7px 22px;
    color: #fff;
}
.box-text-bnner img {
    width: 480px;
}
.box-text-bnner {
    text-align: center;
    margin: 100px 0;
}
.box-text-bnner h4 {
    font-size: 24px;
    padding: 20px 0;
}

//9/08/2021

.main-qty-sec .box:first-child {
    max-width: 200px;
}
.main-qty-sec .box .select {
    position: relative;
    width: 100%;
}
.main-qty-sec .box:last-child span {
    max-width: 15%;
	}
.main-qty-sec .box:last-child #qty {
    max-width: 40%;
	}
.btn-sec .Add_to_cart, .btn-sec .Add_to_wishList {
    width: 23%;
	}
.accordion-title div {
    margin-right: 10px;
}
.accordion-title {
    display: flex;
    align-items: center;
    justify-content: flex-start;
}
.accordin {
    width: 100%;
    float: left;
}
.main-qty-sec .box:last-child{
    margin: 0px 0px 0px !important;
}
.product-slider .slick-slide img {
    display: block;
    margin: 0 auto;
}
.product-slider .slick-slider.slick-initialized {
    width: 100%;
}
.main-qty-sec .box {
    margin: 0 !important;
}
.product-slider-text button.btn.btn-cart {
    background: #00356a;
    color: #fff;
    border-radius: 45px;
    font-size: 18px;
    text-transform: uppercase;
    padding: 8px 20px;
}
.total_selected h4{
	margin: 0px 9px 0px !important;
}
.frequestion-section .thumbnail{
	border: transparent;
}
.frequestion-section .product-firxt {
    margin-right: 10px;
}

// .frequestion-section button.btn.btn-cart{
// 	background: rgb(158 158 158);
// }

//11aug
.accordin .accordion-title {
    padding: 10px;
    font-size: 24px;
    position: relative;
}
.product-slider-text h2 {
    text-align: left;
    font-size: 30px !important;
    margin-bottom: 10px !important;
    margin: 0;
    font-weight: 600;
}
.product-slider-now .slick-slider img {
    width: 300px;
    height: 300px;
}
.product-slider .thumb-slider {
    width: 171px;
    float: left;
    z-index: 1;
    position: relative;
}
.product-slider .thumb-slider .slick-vertical .slick-slide {
    display: flex;
    height: 148px;
    border: 1px solid transparent;
    align-items: center;
    justify-content: center;
    width: 100%!important;
    max-width: 148px;   
}
.product-slider .product-slider-now {
    width: 100%;
    max-width: 500px;
    margin: 0 0 0 auto;
    margin-right: 71px;
}
.container {
    width: 100%!important;
    max-width: 1390px!important;
}
.product-slider .thumb-slider .slick-list {
    padding-top: 24px;
    height: 578px!important;
}
.product-slider .thumb-slider .slick-list .slick-slide {
    margin-bottom: 0;
    margin-top: -4px;
    cursor: pointer;
}
 .product-slider-now .slick-slider img {
    width: 400px;
    height: 400px;
    margin: 0 auto;
    // transform: translateX(63px);
}
.product-slider {
    width: 100%;
    margin: 0 auto;
    padding: 0 24px;
}
.product-section-slider {
    background: #fff;
    margin: 40px 0 0;
}
.product-slider-text {
    margin-top: 25px;
}
.thumb-slider img {
    width: 130px!important;
    height: 135px;
    padding: 0;
    border: 0;
}
// .product-slider .thumb-slider .slick-list .slick-current:after {
//     content: '';
//     position: absolute;
//     width: 140px;
//     height: 140px;
//     border: 1px solid black;
// }
// .product-slider .thumb-slider .slick-list .slick-current {
   
//     position: relative;
// }
.total-text {
    display: flex;
    margin: 30px 0;
}
.frequestion-section .total-cart {
    margin-left: 20px;
    margin-top: 20px;
}
.thumb-slider .slick-slide {
    margin-bottom: -44px;
    margin-top: 32px;
}
.accordion-content {
    padding: 0 10px;
    font-size: 16px !important;
}
.accordin .accordion-title::after {
    position: absolute;
    border-left: 1px solid rgb(0, 0, 0);
    width: 40%;
    height: 2px;
    background: rgb(0, 0, 0);
    content: "";
    right: 0px;
    left: 0px;
    margin: 0px auto 0px 214px;
    top: 27px;
}
a.prev-btn-now {
    margin-right: 10px;
    padding: 10px 39px !important;
}
.frequestion-section h3::after {
    position: absolute;
    content: "";
    border-right: 1px solid #000;
    width: 100%;
    background: #000;
    height: 2px;
    /* margin-left: 26px !important; */
    top: 18px;
    max-width: 230px;
}


///////////////////1600///////////////////
@media(max-width:1600px){
    // .thumb-slider {
    
    //     transform: translateX(-42%);
    // }
     .product-section-banner {
        background: rgba(0, 0, 0, 0) url("/images/product-banner.png") no-repeat scroll 0% 0% / 100%;
        height: 450px;
    }
    
     .study-now {
        background: rgba(0, 0, 0, 0) url("/images/b1.jpg") no-repeat scroll center center / 66%;
        height: 100%;
        padding: 11px;
    }
     .study-now p {
        width: 58%;
    
    }
     .study-now h6 {
        width: 256px;
        margin: 10px auto;
        line-height: 19px;
    }
     .box-right img {
        width: 142px;
    }
     .gar-img img {
        width: 100%;
        margin: 10px 0px;
    }
    .emXdAX .blue-bg {
        height: 100%;
        min-height: 668px;
    }
    }

    @media(max-width:1440px){
        .product-section-banner {
           height:375px;
        }
    }

    @media(max-width:1366px){
        .frequestion-section h3::after {           
            max-width: 132px;
        }   
    }

    @media(max-width:1280px){
        .frequestion-section h3::after {           
            max-width: 132px;
        }
    }

    // .product-slider-now .slick-list {
    //     height: 516px;
    //     background: white;
    //     width: 249px;
    //     text-align: center;
    //     display: flex;
    //     flex-direction: column;
    //     align-items: center;
    //     margin-left: 80px;
    // }
`;

export default ProductPageDesign;