import { useForm } from "react-hook-form";
import { ToastContainer, toast } from 'react-toastify';
import NumberFormat from 'react-number-format';

const Walletsection = ({ profilepageData, router, commission_wallet }) => {


    return (<>
        <div className="wallet_data">

            <ul className="kaire_cash_ul">

                <li><strong>Kaire Cash: </strong></li>

                <li><NumberFormat value={parseFloat(profilepageData?.userdetails[0]?.kaire_cash).toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <div>{value}</div>} /></li>

                <li><button type="button" onClick={() => {
                    router.push({
                        pathname: `/${router.query.page}/user/wallet/`,
                        query: { logtype: "kairecash" }
                    });
                }}>View logs</button></li>

            </ul>
            <ul className="commission_cash_ul">

                <li><strong>Commission Wallet: </strong></li>

                <li><NumberFormat value={parseFloat(commission_wallet).toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <div>{value}</div>} /></li>

                <li><button type="button" onClick={() => {
                    router.push({
                        pathname: `/${router.query.page}/user/wallet/`,
                        query: { logtype: "Commissions" }
                    });
                }}>View logs</button></li>

            </ul>


            <div className="buttton-reem">
                <button type="button" className="btn btn-primary"   >Shop to Redeem</button>

                <button type="button" className="btn btn-primary"   >Redeem to Bank</button>

            </div>
        </div>
        <div className="public_details_section ">
            <>

                <h4 className="title">Public details</h4>
                <div className="profile_first_name">
                    <label className="name"><strong>First Name:</strong></label>
                    {
                        profilepageData?.userdetails[0]?.public_first_name ? profilepageData?.userdetails[0]?.public_first_name : profilepageData?.first_name

                    }
                </div>
                <div className="profile_last_name">
                    <label className="last_name"><strong>Last Name:</strong></label>
                    {profilepageData?.userdetails[0]?.public_last_name
                        ?
                        profilepageData?.userdetails[0]?.public_last_name
                        :
                        profilepageData?.last_name
                    }
                </div>
                <div className="profile_email">
                    <label className="p_emai_lablel"><strong>Email: </strong></label>

                    {profilepageData?.userdetails[0]?.public_email
                        ?
                        profilepageData?.userdetails[0]?.public_email
                        :
                        profilepageData?.email
                    }

                    {/* <input type="checkbox" name="email" onChange={(e) => {
                  SetpublicDetails(e,'email')
              }} /> */}
                </div>

                <div className="profile_phone no">
                    <label className="p_emai_lablel"><strong>Contact Number: </strong></label>

                    {profilepageData?.userdetails[0]?.public_phone_number
                        ?
                        profilepageData?.userdetails[0]?.public_phone_number
                        :
                        profilepageData?.userdetails[0]?.phone_number
                    }

                    {/* <input type="checkbox" name="phone_number" onChange={(e) => {
                  SetpublicDetails(e,'phone_number')
              }} /> */}
                </div>
                {/* <button type="button" className="btn btn-primary" onClick={() => { setEditPublicDetails(true) }}  >Edit Public Details</button> */}
            </>

        </div>
    </>

    )
}
export default Walletsection;