import { set } from "js-cookie";
import useTranslation from "next-translate/useTranslation";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Leftnav from "../../../../Components/Leftnav";
import api from '../../../../api/Apis'
import ProfileEdit from "../../../Components/Profile/ProfileEdit";
import NumberFormat from 'react-number-format';
import Walletsection from "../../../Components/Profile/Walletsection";
import SaveAnalyticsCode from "../../../Components/Profile/SaveAnalyticsCode";
import ImageUpload from "../../../Components/Profile/ImageUpload";

const Profile = ({ validateauth, setshowloader, showprotectedpage }) => {
    const { t } = useTranslation();
    const [changePasswardStatus, setchangePasswardStatus] = useState();
    const [profilepageData, setprofilepageData] = useState();
    const [passwordData, setpasswordData] = useState();
    const [changepasswordresponse, setresponsepassword] = useState();
    const { register, handleSubmit, watch, errors, getValues } = useForm();
    const [LoginToken, setLoginToken] = useState();
    const [editdetails, setEditDetails] = useState(false);
    const [reloaddata, setreloadData] = useState(false);
    const [editpublicdetails, setEditPublicDetails] = useState(false);

    const router = useRouter();

    const [commission_wallet, SetCommissionWallet] = useState()
    // for reset password
    const onSubmit = data => {
        const password = getValues("password");
        const confirmpassword = getValues('confirm_password');
        if (password !== confirmpassword) {
            setchangePasswardStatus('false')
        }
        else {
            setpasswordData(data)
            setchangePasswardStatus('true')
        }

    }
    // load profile page data
    useEffect(() => {
        validateauth()

        if (Object.keys(router?.query).length > 1) {
            router.push('/us/404/')
        }
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        setLoginToken(token)
        const formDataDelete = { old_password: passwordData?.old_password, token: token, new_password: passwordData?.password };
        {
            changePasswardStatus === 'true' && passwordData?.old_password ?

                api.changeUserPassword(formDataDelete).then(res => {
                    setresponsepassword(res.data.message);
                })
                :
                api.getProfilePageData(token).then(res => {
                    if (res?.data?.code === 1) {
                        setprofilepageData(res.data.user);
                        SetCommissionWallet(res.data.commission_wallet)
                    }
                })
        }
        api.getProfilePageData(token).then(res => {
            if (res?.data?.code === 1) {
                setprofilepageData(res.data.user);
                SetCommissionWallet(res.data.commission_wallet)
                setreloadData(false)
            }
        })
    }, [changePasswardStatus, reloaddata]);


    console.log('showprotectedpage profile', showprotectedpage)
    return (<>
        <Head>
            <title>Profile</title>
        </Head>
        {showprotectedpage == true &&

            <div className="mainorder-detail-sec">
                <div className="profile-page-main">
                    <div className="container">
                        <div className="row">
                            <Leftnav />
                            <div className="col-md-9">
                                <div className="right-side">
                                    <h3 className="title">Personal details</h3>
                                    <ImageUpload setshowloader={setshowloader} profilepageData={profilepageData} />

                                    <Walletsection
                                        profilepageData={profilepageData}
                                        router={router}
                                        commission_wallet={commission_wallet}
                                    />

                                    <br /> <br />
                                    <div className="row">
                                        <div className="col-md-6">
                                            {editdetails === false ?

                                                <>
                                                    <div className="pro-fil-user">

                                                        <div className="left-profile-data">
                                                            <div className="profile_first_name">
                                                                <label className="name"><strong>First Name:</strong></label>
                                                                {profilepageData?.first_name && profilepageData.first_name}

                                                            </div>
                                                            <div className="profile_last_name">
                                                                <label className="last_name"><strong>Last Name:</strong></label>
                                                                {profilepageData?.last_name && profilepageData.last_name}

                                                            </div>
                                                            <div className="profile_email">
                                                                <label className="p_emai_lablel"><strong>Email: </strong></label>

                                                                {profilepageData?.email && profilepageData.email}

                                                            </div>

                                                            {/* <div className="profile_age">
                                                            <label className="p_age_lablel"><strong>Age: </strong></label>

                                                            25

                                                        </div> */}
                                                            {/* <div className="profile_gender">
                                                            <label className="p_ender_lablel"><strong>Gender: </strong></label>

                                                            Male

                                                        </div> */}
                                                            <div className="profile_Address">
                                                                <label className="p_address_lablel"><strong>Street Address: </strong></label>

                                                                {profilepageData?.email && profilepageData.user_addresses[0]?.street_address_1}
                                                            Complex
                                                        </div>
                                                            <div className="profile_city">
                                                                <label className="p_city_lablel"><strong>Town/City: </strong></label>

                                                                {/* {profilepageData?.email && profilepageData.email} */}
                                                                                Chandigarh
                                                        </div>
                                                            <div className="profile_State">
                                                                <label className="p_state_lablel"><strong>State: </strong></label>

                                                            Chandigarh

                                                        </div>
                                                            {/* <div className="profile_zipcode">
                                                            <label className="p_zipcode_lablel"><strong>ZipCode: </strong></label>

                                                            {profilepageData?.email && profilepageData.email}

                                                        </div> */}
                                                            {/* <div className="profile_country">
                                                            <label className="p_country_lablel"><strong>Country: </strong></label>

                                                            {profilepageData?.email && profilepageData.email}

                                                        </div> */}
                                                            <div className="profile_number">
                                                                <label className="p_nu,ber_lablel"><strong>Phone Number: </strong></label>

                                                                {profilepageData?.userdetails[0]?.phone_number && profilepageData.userdetails[0].phone_number}

                                                            </div>

                                                            <div className="referral_code">
                                                                <label className="referral_code_lable"><strong>Referral code: </strong></label>
                                                                {profilepageData?.userdetails[0]?.referral_code}

                                                            </div>
                                                        </div>
                                                        <button type="button" className="btn btn-primary" onClick={() => { setEditDetails(true) }}  >Edit</button>
                                                    </div>




                                                </>
                                                :
                                                <ProfileEdit setreloadData={setreloadData} setEditDetails={setEditDetails} LoginToken={LoginToken} profilepageData={profilepageData} />
                                            }
                                        </div>
                                        <div className="col-md-6">
                                            {changepasswordresponse && <h3 className="title">{changepasswordresponse}</h3>}
                                            {changePasswardStatus === 'new' || changePasswardStatus === 'false' ? <>
                                                <form onSubmit={handleSubmit(onSubmit)}>
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <div className="form-group">
                                                                <label htmlFor="exampleInputPassword1" className="form-label"> Old Password</label>
                                                                <input type="password" className="form-control" name="old_password" id="exampleInputPassword2" ref={register({ required: true })} />
                                                            </div>
                                                            {errors.old_password && <span>This field is required</span>}


                                                        </div>
                                                        <div className="col-md-12">
                                                            <div className="form-group">
                                                                <label htmlFor="exampleInputPassword1" className="form-label">New Password</label>
                                                                <input type="password" className="form-control" name="password" id="exampleInputPassword1"
                                                                    ref={register({
                                                                        required: "This field is required",
                                                                        minLength: {
                                                                            value: 8,
                                                                            message: "Password must have at least 8 characters"
                                                                        },
                                                                        pattern: {
                                                                            value: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,32}$/,
                                                                            message: "Enter secure password that includes atleast 1 upercase letter, 1 number"
                                                                        }
                                                                    })}
                                                                />
                                                            </div>
                                                            {errors.password && <span>{errors.password.message}</span>}

                                                        </div>
                                                        <div className="col-md-12">
                                                            <div className="form-group">
                                                                <label htmlFor="exampleInputPassword1" className="form-label">Confirm Password</label>
                                                                <input type="password" className="form-control" name="confirm_password" id="exampleInputPassword3" ref={register({ required: true })} />

                                                            </div>
                                                            {errors.confirm_password && <span>This field is required</span>}
                                                            {changePasswardStatus === "false" && <span>Password and confirm password not match</span>}
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <button type="submit" className="btn btn-primary">Update Password</button>
                                                    </div>
                                                </form>
                                            </>
                                                :
                                                editdetails === false &&
                                                <button type="button" className="btn btn-primary" onClick={() => { setchangePasswardStatus('new') }}  >Change Password</button>
                                            }
                                        </div>
                                    </div>



                                </div>

                                {/* <SaveAnalyticsCode /> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        }
    </>);
}
export default Profile;