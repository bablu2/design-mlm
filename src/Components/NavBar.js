import Link from "next/link";
import React, { useEffect, useState } from "react";
import Image from 'next/image'
import img from '../Images/logo.jpg'
import { useRouter } from "next/router";
import { route } from "next/dist/next-server/server/router";
import Login from "../pages/[page]/login";
import Modal from '@material-ui/core/Modal';
import Signup from "../pages/[page]/signup";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import NextNProgress from 'nextjs-progressbar'
import Head from "next/head";
import NumberFormat from 'react-number-format';
import { FaShoppingCart, FaHeart, FaUser, FaSignOutAlt, FaBell } from "react-icons/fa";
import api from '../api/Apis'

const NavBar = props => {

    const router = useRouter();
    const store = (router?.query?.page) ? router?.query?.page : 'us'

    const showlogin = () => {
        props.setoldpath(router.asPath)
        router.push(`/us/login`)
    }

    return (
        <div className="header-section">
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <nav className="navbar navbar-default">
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                    <Link className="nav-link" exact="true" href="/" >
                        <a className="navbar-brand">
                            <img src="/images/main-logo.png" />
                        </a>
                    </Link>
                </div>
                <div className="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul className="nav navbar-nav navbar-left">
                        <li><a href="#">Antioxidants</a></li>
                        <li><a href="#">Keto Kaire </a></li>
                        <li><a href="#"> Brain Health</a></li>
                        <li><a href="#">Diet & Weight Loss</a></li>
                        <li><a href="#">Nutritional Support</a></li>
                    </ul>
                    <ul className="nav navbar-nav navbar-right" onClick={() => { props?.updatecartdata(true) }}>
                        {/* my account section with pop up html */}
                        <li>
                            {props.isLogin === true ?
                                <div className="dashboard_div">
                                    <Link className="nav-link" exact="true" href={`/${store}/user/dashboard`}>
                                        <a>
                                            <div className="account_title">My Account</div>
                                            <div className="account_icon"><FaUser />
                                            </div>
                                        </a>
                                    </Link>
                                </div>
                                :
                                <div className="login_signin">
                                    <Link className="nav-link" exact="true" href={`/${store}/signup`}>
                                        <a>
                                            <div className="account_title">Register</div>
                                        </a>
                                    </Link>
                                    <a onClick={showlogin} style={{ cursor: "pointer" }}>Login</a>
                                </div>
                            }
                        </li>

                        {/* view Cart with pop up html */}
                        <li className="view_cart_section">
                            <Link className="" exact="true" href={`/${store}/cart/viewCart`}>
                                <div className="dashboard_div">
                                    <a>
                                        <div className="cart_title" style={{
                                            cursor: 'pointer'
                                        }}>
                                            View Cart:
                                        </div>
                                        <div className="icon-box-cart">
                                            <FaShoppingCart />
                                            {props?.counts?.products?.length > 0 &&
                                                <span>{props?.counts?.products?.length > 0 ? props?.counts?.products?.length : 0}</span>
                                            }
                                        </div>
                                    </a>
                                </div>
                            </Link>

                            <ul className="dropdown-popup_viewCart"> {/* pop up to show on viewCart */}
                                {props?.counts?.products?.map((productdata, index) => {
                                    return (
                                        <>
                                            <li className="" key={index + 1}>
                                                <div className="view_cart_div">
                                                    {productdata?.product?.product_images[0]?.image &&
                                                        <img src={`${process.env.API_URL}${productdata.product?.product_images[0]?.image}`} />}
                                                </div>
                                                <div className="txt" >
                                                    {productdata?.variant === null ?
                                                        <>
                                                            {productdata?.product.name} <br />
                                                                    Qty:{productdata?.quantity} <br />

                                                            <NumberFormat
                                                                value={parseFloat(props?.counts?.is_autoship_user === 'True' ?
                                                                    productdata?.product?.autoship_cost_price * productdata?.quantity
                                                                    :
                                                                    productdata?.product?.cost_price * productdata?.quantity).toFixed(2)}
                                                                displayType={'text'}
                                                                thousandSeparator={true}
                                                                prefix={'$'}
                                                                renderText={value => <div>{value}</div>} />
                                                        </>
                                                        :
                                                        <>
                                                            {productdata?.product.name}- {productdata?.variant?.name} <br />
                                                                    Qty:{productdata?.quantity} <br />

                                                            <NumberFormat
                                                                value={parseFloat(props?.counts?.is_autoship_user === 'True' ?
                                                                    productdata?.variant?.autoship_cost_price * productdata?.quantity
                                                                    :
                                                                    productdata?.variant?.cost_price * productdata?.quantity).toFixed(2)}
                                                                displayType={'text'}
                                                                thousandSeparator={true}
                                                                prefix={'$'}
                                                                renderText={value => <div>{value}</div>} />
                                                        </>
                                                    }
                                                </div>
                                            </li>
                                        </>
                                    )
                                })}
                                <li className="">
                                    <Link className="" exact="true" href={`/${store}/cart/viewCart`}>
                                        <a className="">View cart</a>
                                    </Link>
                                </li>
                            </ul>
                        </li>

                        {/* Notfication icon with pop up  */}
                        <li className="notifications">
                            {props.isLogin === true &&
                                <>
                                    <div className="">
                                        <FaBell />
                                        {props.usernotifications?.length > 0 &&
                                            <span className="count label label-warning" id="quick-notifications-menu">
                                                {props.usernotifications?.length > 0 ? props.usernotifications?.length : 0}
                                            </span>
                                        }
                                    </div>
                                    <ul className="dropdown-menu-for-cart"> {/*pop for notification */}
                                        {props.usernotifications?.map((notification, index) => {
                                            return (<>
                                                {notification?.table_name === "Order" ?
                                                    <li key={index + 1} className="nav-item" onClick={() => {
                                                        router.push({
                                                            pathname: `/${store}/user/autoshiporder`,
                                                            query: { orderid: notification?.instance_id },
                                                        })
                                                    }}>
                                                        {notification?.action_performed}
                                                    </li>
                                                    :
                                                    <li key={index + 1}>
                                                        {notification?.action_performed}
                                                    </li>
                                                }
                                            </>)
                                        })}
                                    </ul>
                                </>
                            }
                        </li>

                        {/* wishlist icon with popup html */}
                        <li className="wishlist_section">
                            {props.isLogin === true &&
                                <>
                                    <Link className="" exact="true" href={`/${store}/wishlist/wishlist`}>
                                        <a>
                                            <div className="icon-box">
                                                <FaHeart />
                                                {props?.counts?.wishlist?.length > 0 &&
                                                    <span>{props?.counts?.wishlist?.length > 0 ? props?.counts?.wishlist?.length : 0}</span>
                                                }
                                            </div>
                                        </a>
                                    </Link>
                                    <ul className="dropdown-menu-wishlist dropdown-popup_viewCart" > {/* pop of wishlist */}
                                        {props?.counts?.wishlist?.map((productdata, index) => {
                                            return (
                                                <li className="" key={index}>
                                                    <div className="view_cart_div">
                                                        {productdata?.product?.product_images[0]?.image && <img src={`${process.env.API_URL}${productdata.product?.product_images[0]?.image}`} />}
                                                    </div>
                                                    <div className="txt" >
                                                        {productdata?.variant === null ?
                                                            <>
                                                                {productdata?.product.name} <br />
                                                                             Qty:{productdata?.product?.quantity} <br />

                                                                <NumberFormat
                                                                    value={parseFloat(props?.counts?.is_autoship_user === 'True' ?
                                                                        productdata?.product?.autoship_cost_price * productdata?.quantity
                                                                        :
                                                                        productdata?.product?.cost_price * productdata?.quantity).toFixed(2)}
                                                                    displayType={'text'}
                                                                    thousandSeparator={true}
                                                                    prefix={'$'}
                                                                    renderText={value => <div>{value}</div>} />
                                                            </>
                                                            :
                                                            <>
                                                                {productdata?.product.name}- {productdata?.variant?.name} <br />
                                                                    Qty:{productdata?.variant?.quantity} <br />
                                                                <NumberFormat value={parseFloat(props?.counts?.is_autoship_user === 'True' ?
                                                                    productdata?.variant?.autoship_cost_price * productdata?.quantity
                                                                    :
                                                                    productdata?.variant?.cost_price * productdata?.quantity).toFixed(2)}
                                                                    displayType={'text'}
                                                                    thousandSeparator={true}
                                                                    prefix={'$'}
                                                                    renderText={value => <div>{value}</div>} />
                                                            </>}
                                                    </div>
                                                </li>
                                            )
                                        })}
                                        <li className="">
                                            <Link className="" href={`/${store}/wishlist/wishlist`}>
                                                <a className="">View Wishlist</a>
                                            </Link>
                                        </li>
                                    </ul>
                                </>
                            }
                        </li>

                        {/* LOgout icon with pop up  */}
                        <li className="logout_icon">
                            {props.isLogin === true &&
                                <FaSignOutAlt onClick={props.handleLogout} />
                            }
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    );
}
export default NavBar;