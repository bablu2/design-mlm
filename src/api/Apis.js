import axios from "axios";
import cookieCutter from 'cookie-cutter'

//register api that use when user register path where it used /page/[page]/signup.js
function signUp(PostData = {}) {
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
  };
  return axios.post(process.env.sinUpApi, PostData, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}
//api for login user.  path where it used /page/[page]/login.js
function loginUser(PostData = {}) {
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
  };
  return axios.post(process.env.loginApi, PostData, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}
//api for logout user  path where it used /page/_app.js
function logoutApi(PostData) {
  const headers = {
    'Authorization': PostData,

  };
  const data = '';
  return axios.post(process.env.logoutApi, data, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}

// api for add product to cart  path where it used /page/product/[name]/[id].js
function addToCart(PostData = {}) {
  const headers = {
    'Authorization': `${PostData.token}`
  };



  const PostDataa = { "cookie_id": cookieCutter.get('sessionkey'), "product_id": +(PostData.product_id), "variant_id": PostData?.variant_id ? +(PostData.variant_id) : null, "quantity": +(PostData.quantity), "is_autoship": PostData?.is_autoship }


  return axios.post(process.env.addToCart, PostData?.datas ? PostData.datas : PostDataa, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}


//api for get banner details and categoried listing according to store , path where it used /page/index.js
function getBanners(PostData) {
  const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  };

  return axios.get(`${process.env.getBanners}?slug=${PostData}`, { headers }).then(function (response) {
    return response;

  }).catch((error) => {

    return error.response;
  });
}
function getHomepageContent(PostData) {
  const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  };

  return axios.get(`${process.env.getHomepageContent}?slug=${PostData}`, { headers }).then(function (response) {
    return response;

  }).catch((error) => {

    return error.response;
  });
}

//api for display cart data  path where it used /page/cart/viewcart.js
function getAllCartProduct(PostData) {
  const headers = {

    'Authorization': PostData
  };

  return axios.get(`${process.env.getAllCartProduct}/?cookie_id=${cookieCutter.get('sessionkey')}`, { headers }).then(function (response) {
    // return axios.get(process.env.getAllCartProduct, { headers }).then(function (response) {

    return response;
  }).catch((error) => {

    return error.response;
  });
}


// api for update qty in cart  path where it used /page/cart/viewcart.js
function updateProductQty(PostData = {}) {
  const headers = {

    'Authorization': PostData
  };

  return axios.get(process.env.getAllCartProduct, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}


//api for delete product from cart, path where it used /page/cart/viewcart.js
function deleteProductFromAddToCart(PostData = {}) {
  const headers = {

    'Authorization': PostData.token,
  };
  const PostDataa = { "cookie_id": cookieCutter.get('sessionkey'), "product_id": PostData.product_id, variant_id: PostData?.variant_id ? PostData.variant_id : null }

  return axios.post(process.env.deleteProductFromAddToCart, PostDataa, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}

// api for get list of address of login user , path where it used /page/checkout/addressList.js
function manageAddress(PostData) {
  const headers = {
    'Authorization': PostData
  };

  return axios.get(process.env.manageAddress, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}

// api for save user address path where it used /page/checkout/address.js
function saveAddress(PostData = {}) {
  const headers = {
    'Authorization': PostData.token
  };
  const PostDataa = { "data": PostData.data, "operation": PostData.address_type }

  return axios.post(process.env.manageAddress, PostDataa, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}

// api for delete address ,path where it used /page/checkout/addressList.js



// api for get profile related data like persional,address list,order list, path where it used /page/user/profile.js
function getProfilePageData(PostData) {
  const headers = {

    'Authorization': PostData
  };

  return axios.get(process.env.getProfilePageData, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}
// api for change user login password path where it used /page/user/profile.js
function changeUserPassword(PostData = {}) {
  const headers = {
    'Authorization': PostData.token
  };
  const PostDataa = { "old_password": PostData.old_password, "new_password": PostData.new_password }

  return axios.post(process.env.changeUserPassword, PostDataa, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}

// api for add product to wishlist path where it used /page/product/[name]/[id].js

function addToWishlist(PostData = {}) {
  const headers = {
    'Authorization': `${PostData.token}`
  };
  const PostDataa = { "product_id": +(PostData.product_id), "variant_id": PostData?.variant_id ? +(PostData.variant_id) : null, "quantity": +(PostData.quantity) }

  return axios.post(process.env.addToWishlist, PostDataa, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}

// api for show product listing in wishlist path where it used /page/wishlist/wishlist.js
function getAllWishListProduct(PostData) {
  const headers = {

    'Authorization': PostData
  };

  return axios.get(process.env.getAllWishListProduct, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}

// api for delete product from wish list path where it used /page/wishlist/wishlist.js
function deleteProductFromwishlist(PostData = {}) {
  const headers = {

    'Authorization': PostData.token
  };
  const PostDataa = { "product_id": +(PostData.product_id), "variant_id": +(PostData?.variant_id) ? +(PostData?.variant_id) : null }

  return axios.post(process.env.deleteProductFromwishlist, PostDataa, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}


//api for get banner details and categoried listing according to store , path where it used /page/index.js
function getAllProduct(PostData) {
  const headers = {
    'Accept': 'application/json',
    'Authorization': localStorage.getItem('Token')

  };

  return axios.get(`${process.env.getAllProduct}?slug=${PostData}`, { headers }).then(function (response) {
    return response;

  }).catch((error) => {

    return error.response;
  });
}
// filter product
function filterProduct(PostData, serchdata) {
  const headers = {
    'Accept': 'application/json',
    'Authorization': localStorage.getItem('Token')

  };

  return axios.get(`${process.env.getAllProduct}?slug=${PostData}&search=${serchdata}`, { headers }).then(function (response) {
    return response;

  }).catch((error) => {

    return error.response;
  });
}
//api for get banner details and categoried listing according to store , path where it used /page/index.js
function getProductByCategories(PostData) {
  const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  };

  return axios.get(`${process.env.getProductByCategories}?category_id=${PostData}&search=`, { headers }).then(function (response) {
    return response;

  }).catch((error) => {

    return error.response;
  });
}

//api for get banner details and categoried listing according to store , path where it used /page/index.js
function getProductByProductid(PostData) {
  const headers = {
    'Accept': 'application/json',
    'Authorization': localStorage?.getItem('Token') ? localStorage?.getItem('Token') : ''

  };

  return axios.get(`${process.env.getProductByProductid}?product_id=${PostData}`, { headers }).then(function (response) {
    return response;

  }).catch((error) => {

    return error.response;
  });
}

function getProductByVariantid(PostData) {
  const headers = {
    'Accept': 'application/json',
    'Authorization': localStorage.getItem('Token')

  };

  return axios.get(`${process.env.getProductByVariantid}?variant_id=${PostData}`, { headers }).then(function (response) {
    return response;

  }).catch((error) => {

    return error.response;
  });
}

// Create order

function createOrder(PostData = {}, token) {
  const headers = {

    'Authorization': token
  };

  return axios.post(`${process.env.createOrder}`, PostData, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}

// varify copan
function verifyCopan(PostData = {}, token) {
  const headers = {

    'Authorization': token
  };

  return axios.post(process.env.verifyCopan, PostData, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}
// get all category
function getAllCategory(PostData) {
  const headers = {
    'Accept': 'application/json',
    // 'Authorization': localStorage?.getItem('Token') ? localStorage.getItem('Token') : ''
  };

  return axios.get(`${process.env.getAllCategory}?slug=${PostData}`, { headers }).then(function (response) {
    return response;

  }).catch((error) => {

    return error.response;
  });
}

// update cart quantity

function updateCart(PostData = {}) {
  const headers = {
    'Authorization': `${PostData.token}`
  };

  const PostDataa = { "product_id": +(PostData.product_id), "variant_id": PostData?.variant_id ? +(PostData.variant_id) : null, "quantity": +(PostData.quantity) }
  return axios.post(process.env.updateCart, PostData?.datas ? PostData.datas : PostDataa, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}


// api for get user getUserOrder list

function getUserOrder(PostData) {
  const headers = {

    'Authorization': PostData
  };

  return axios.get(process.env.getUserOrder, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}
// api for get user address details

function getAddressDetails(PostData = {}) {
  const headers = {

    'Authorization': `${PostData.token}`
  };

  return axios.get(`${process.env.getAddressDetails}?address_id=${PostData.address_id}`, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}

// get specific oder details
function GetOrderDetail(PostData = {}) {
  const headers = {

    'Authorization': `${PostData.token}`
  };

  return axios.get(`${process.env.GetOrderDetail}?order_id=${PostData.order_id}`, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}

// delete address

function deleteAddress(PostData, token) {
  const headers = {
    'Authorization': token
  };


  return axios.post(process.env.deleteAddress, PostData, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}

// re-pace same order that show in orderlist 

function reorderProducts(PostData = {}) {
  const headers = {
    'Authorization': `${PostData.token}`
  };

  const PostDataa = { "order_id": +(PostData?.order_id), "cookie_id": cookieCutter.get('sessionkey') }
  return axios.post(process.env.reorderProducts, PostDataa, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}

//get addresslist

function getAddressList(token) {
  const headers = {

    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getAddressList}`, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}


// api for get user getUserAutoShipOrder list

function autoshipOrderHistory(PostData) {
  const headers = {

    'Authorization': PostData
  };

  return axios.get(process.env.autoshipOrderHistory, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}



// get specific autoshipoder details
function autoshipOrderById(PostData = {}) {
  const headers = {

    'Authorization': `${PostData.token}`
  };

  return axios.get(`${process.env.autoshipOrderById}?order_id=${PostData.order_id}`, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}


// update Autoship

function autoshipUpdate(PostData = {}, token) {
  const headers = {
    'Authorization': `${token}`
  };

  // const PostDataa = { "product_id": +(PostData.product_id),"variant_id": PostData?.variant_id ? +(PostData.variant_id):null, "quantity": +(PostData.quantity) }
  return axios.post(process.env.autoshipUpdate, PostData, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}


// delete Autoship
function autoshipDelete(PostData, token) {
  const headers = {
    'Authorization': token
  };


  return axios.post(process.env.autoshipDelete, PostData, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}

// delete autoshipSkip
function autoshipSkip(PostData, token) {
  const headers = {
    'Authorization': token
  };


  return axios.post(process.env.autoshipSkip, PostData, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}


// api for Update user address path where it used /page/checkout/address.js
function updateAddress(PostData = {}) {
  const headers = {
    'Authorization': PostData.token
  };
  const PostDataa = { "data": PostData.data }

  return axios.post(process.env.updateAddress, PostDataa, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}




// get notification details

function getUserNotifications(PostData) {
  const headers = {

    'Authorization': `${PostData}`
  };

  return axios.get(`${process.env.getUserNotifications}`, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}

// delete autoship order product

function autoshipProductDelete(PostData = {}, token) {
  const headers = {

    'Authorization': `${token}`
  };

  return axios.post(`${process.env.autoshipProductDelete}`, PostData, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}




// update Autoship

function autoshipProductUpateproduct(PostData = {}, token) {
  const headers = {
    'Authorization': `${token}`
  };

  // const PostDataa = { "product_id": +(PostData.product_id),"variant_id": PostData?.variant_id ? +(PostData.variant_id):null, "quantity": +(PostData.quantity) }
  return axios.post(process.env.autoshipProductUpateproduct, PostData, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}

// update Autoship address

function addressUpdate(PostData = {}, token) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.post(process.env.addressUpdate, PostData, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}

// reset password
function resetPassword(PostData = {}, token) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.post(process.env.resetPassword, PostData, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}

// Cancel Order
function cancelOrder(PostData = {}, token) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.post(process.env.cancelOrder, PostData, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}

// Refund Order
function refundOrder(PostData = {}, token) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.post(process.env.refundOrder, PostData, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}


// Api for get Bundle product and use in order refund section
function getBundleProduct(PostData, token) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getBundleProduct}/?product_id=${PostData}`, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}
// Api for get downline user an d used in Dashboard

function getDownlineUsers(token) {
  const headers = {

    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getDownlineUsers}`, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}


// redeemKaireCash Order
function redeemKaireCash(PostData, token) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getBundleProduct}/?order_total=${PostData}`, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}


// getRefundHistory Order
function getRefundHistory(PostData, token) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getRefundHistory}/?order_id=${PostData}`, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}

// getCommissionReport 
function getCommissionReport(token, range) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getCommissionReport}?value=${range}`, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}



// getRefundHistory Order
function getCommissionsFilter(PostData = {}, token) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getCommissionsFilter}?value=${PostData?.filter_by_date}&from_date=${PostData?.from_date ? PostData.from_date : ''}&till_date=${PostData?.til_date ? PostData.til_date : ''}&search_on=order_id&search_value=${PostData?.order_id}&status_filter=${PostData?.status ? PostData?.status : ''}`, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}


// getRefundHistory Order
function getRefundReport(token) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getRefundReport}`, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}




// getRefundReportFilter Order
function getRefundReportFilter(PostData = {}, token) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getRefundReportFilter}?date_filter=${PostData?.filter_by_date}&from_date=${PostData?.from_date ? PostData.from_date : ''}&till_date=${PostData?.til_date ? PostData.til_date : ''}&search_on=order_id&order_id=${PostData?.order_id}`, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}



// getRefundHistory Order
function getMyProfileDetails(token) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getMyProfileDetails}`, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}

// getDashboardCommissions Order
function getDashboardCommissions(range, token, PostData = {}) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getDashboardCommissions}?value=${range}&from_date=${PostData?.from_date ? PostData.from_date : ''}&till_date=${PostData?.til_date ? PostData.til_date : ''}&search_on=order_id&order_id=${PostData?.order_id}`, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}

// GetCommissionsApproved Order
function GetCommissionsApproved(token, range, PostData = {}) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.get(`${process.env.GetCommissionsApproved}?value=${range}&from_date=${PostData?.from_date ? PostData.from_date : ''}&till_date=${PostData?.til_date ? PostData.til_date : ''}&search_value=${PostData?.order_id ? PostData?.order_id : ''}`, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}



// LoginCheck Order
function LoginCheck(token) {
  const headers = {
    'Authorization': localStorage.getItem('Token') //`${token}`
  };

  return axios.get(`${process.env.LoginCheck}`, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}



// LoginCheck Order
function LoginCheckByurl(token) {
  const headers = {
    'Authorization': `${token}` //localStorage.getItem('Token') //`${token}`
  };

  return axios.get(`${process.env.LoginCheck}`, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}

// get specific oder details
function getOrderDetailForRefund(PostData = {}) {
  const headers = {

    'Authorization': `${PostData.token}`
  };

  return axios.get(`${process.env.getOrderDetailForRefund}?order_id=${PostData.order_id}`, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}



// update profileUpdate

function profileUpdate(PostData = {}, token) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.post(process.env.profileUpdate, PostData, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}



// GetCommissionsApproved Order
function getAllTranaction(token, range, PostData = {}) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getAllTranaction}?value=${range}&from_date=${PostData?.from_date ? PostData.from_date : ''}&till_date=${PostData?.til_date ? PostData.til_date : ''}`, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}


// Api for get downline user an d used in Dashboard

function getNewDownlineUsers(token) {
  const headers = {

    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getNewDownlineUsers}`, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}


// getDownlineUserAddress
function getDownlineUserAddress(token, orderID) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getDownlineUserAddress}?user_id=${orderID}`, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}


// getKaireCashTranaction
function getKaireCashTranaction(token, range, PostData = {}) {
  const headers = {
    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getKaireCashTranaction}?value=${range}&from_date=${PostData?.from_date ? PostData.from_date : ''}&till_date=${PostData?.til_date ? PostData.til_date : ''}`, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}

// Api for get dashboard news an d used in Dashboard

function getDashboardNews(token) {
  const headers = {

    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getDashboardNews}`, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}



// update profileUpdate

function profileImageUpload(PostData, token) {
  const headers = {
    'Authorization': `${token}`,
    // 'Content-Disposition': 'attachment; filename="My Report.png"'
    "Content-Type": "multipart/related; boundary=xy1z"
    // "Content- Disposition": ` form-data; name = "aFile"; filename = ${PostData.file12.name} `

  };
  const formData = new FormData();

  // Update the formData object
  formData.append(
    "file",
    PostData
  );
  // console.log(PostData.file.name, 'ok')
  return axios.post(process.env.profileImageUpload, formData, { headers }).then(function (response) {
    return response;
  })
    .catch((error) => {

      return error.response;
    });
}



// Api for get getDocuments

function getDocuments(token) {
  const headers = {

    'Authorization': `${token}`
  };

  return axios.get(`${process.env.getDocuments}`, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}


// Api for get getDocuments

function downloadDoc(url) {
  const headers = {

    'Authorization': ``
  };

  return axios.get(`${url}`, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}

//multi Add to cart api

function multiProductAddToCart(token, formData) {
  const headers = {
    'Authorization': `${token}`,
    'Content-Type': "application/json"
  };

  return axios.post(`${process.env.multiProductAddToCart}`, formData, { headers }).then(function (response) {
    return response;
  }).catch((error) => {

    return error.response;
  });
}

const api = {
  signUp,
  getBanners,
  loginUser,
  addToCart,
  logoutApi,
  getAllCartProduct,
  updateProductQty,
  deleteProductFromAddToCart,
  manageAddress,
  saveAddress,
  deleteAddress,
  getProfilePageData,
  changeUserPassword,
  addToWishlist,
  getAllWishListProduct,
  deleteProductFromwishlist,
  getAllProduct,
  getProductByCategories,
  getProductByProductid,
  getProductByVariantid,
  createOrder,
  verifyCopan,
  getAllCategory,
  updateCart,
  getUserOrder,
  getAddressDetails,
  GetOrderDetail,

  reorderProducts,
  getAddressList,
  autoshipOrderHistory,
  autoshipOrderById,
  autoshipUpdate,
  autoshipDelete,
  autoshipSkip,
  updateAddress,
  getUserNotifications,
  autoshipProductDelete,
  autoshipProductUpateproduct,
  addressUpdate,
  resetPassword,
  refundOrder,
  cancelOrder,
  getBundleProduct,
  getDownlineUsers,
  redeemKaireCash,
  getRefundHistory,
  getCommissionReport,
  getCommissionsFilter,
  getRefundReport,
  getRefundReportFilter,
  getMyProfileDetails,
  getDashboardCommissions,
  GetCommissionsApproved,
  LoginCheck,
  getOrderDetailForRefund,
  profileUpdate,
  getAllTranaction,
  getNewDownlineUsers,
  getDownlineUserAddress,
  getKaireCashTranaction,
  getDashboardNews,
  filterProduct,
  LoginCheckByurl,
  profileImageUpload,
  getDocuments,
  downloadDoc,
  getHomepageContent,
  multiProductAddToCart
}
export default api