
import styles from '../../styles/Home.module.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import icon from '../../public/favicon.ico'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { useRouter } from 'next/router';
import api from '../api/Apis'
import { useEffect, useState } from 'react';
import Home from './Components/Home/Home'
export default function Index() {
  const route = useRouter();
  const store = "us";
  const [homePageData, sethomePageData] = useState();
  useEffect(() => {
    api.getBanners(store).then(res => {
      sethomePageData(res?.data)
    })
    api.getHomepageContent(store).then(res => {
      // sethomePageData(res?.data)
      console.log('getHomepageContent', res?.data)
    })
  }, []);

  return <Home styles={styles} icon={icon} homePageData={homePageData} />
}
