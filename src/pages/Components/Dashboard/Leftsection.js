
import Link from "next/link";
import { FaUser, FaEnvelope, FaPhoneAlt } from "react-icons/fa";
import NumberFormat from "react-number-format";
import { GrDiamond } from "react-icons/gr";
import Moment from 'moment';
import moment from "moment"
import { useRouter } from "next/router";

const Leftsection = ({ newsdata, profileData, documentdata }) => {
    const router = useRouter()

    return (
        <div className="col-md-3">
            <div className="leftsec">
                <div className="usericon-a">
                    <div className="usericon">

                        {profileData?.user_data?.user_profile?.image ? <h3><img src={`${process.env.DOC_URL}/media/${profileData?.user_data?.user_profile?.image}`} alt="" width="100" /></h3> : <FaUser />}

                    </div>
                    <div className="user-Reser">
                        <h5 className="title">{profileData?.user_data?.first_name} {profileData?.user_data?.last_name}</h5>
                    </div>


                    <div className="icons">

                        <Link className="nav-link" href={`mailto:${profileData?.user_data?.email}`}>
                            <a><FaEnvelope /></a>
                        </Link>
                        <Link className="nav-link" href={`callto:${profileData?.user_data?.userdetails[0]?.phone_number}`}>
                            <a><FaPhoneAlt /></a>
                        </Link>

                    </div>
                </div>

                <div className="deatis-info">
                    <div className="dt-jned"><span>Date Joined : </span>
                        {profileData?.user_data?.date_joined && moment(profileData?.user_data?.date_joined).format(`${process.env.Date_Format}`)}

                    </div>
                    <div className="dt-jned"><span>Referal Code : </span> {profileData?.user_data?.userdetails[0]?.referral_code}</div>
                    <div className="dt-jned"><span>Referal URL :  </span>Demourl</div>
                    <div className="dt-jned"><span>Autoship Activated :  </span> {profileData?.user_data?.userdetails[0]?.is_autoship_user}</div>


                </div>
                <div className="current-part">
                    <div className="currentrank">
                        <div className="current-rank-left">
                            <h3 className="title1-heading">Paid as rank:</h3>

                            {

                                profileData?.ranks?.map((ranks) => {
                                    return (
                                        +profileData?.user_data?.userdetails[0]?.paid_as_rank === ranks.id &&
                                        <h3 className="title1 " key={ranks?.rank_name}>{ranks?.rank_name}</h3>

                                    )
                                })

                            }

                        </div>
                        <div className="current-rank-right">
                            <h3 className="title1-heading">Qualified as rank:</h3>

                            <h3 className="title1 ">
                                {

                                    profileData?.ranks?.map((ranks) => {
                                        return (
                                            +profileData?.user_data?.userdetails[0]?.qualified_as_rank === ranks.id &&
                                            ranks?.rank_name

                                        )
                                    })

                                }

                            </h3>
                        </div>

                    </div>
                </div>
                <div className="Distributor row">
                    <GrDiamond />

                </div><br /><br />
                <div className="total_sale">
                    <div className="Toal-Sale"><strong>Total Sales : </strong>

                        <NumberFormat
                            value={parseFloat(profileData?.total_sales).toFixed(2)}
                            displayType={'text'} thousandSeparator={true} prefix={'$'}
                            renderText={value => <div> {value} </div>} />
                    </div>
                    <div className="Toal-commission"><strong>Total Commissions : </strong>

                        <NumberFormat
                            value={parseFloat(profileData?.total_commission).toFixed(2)}
                            displayType={'text'} thousandSeparator={true} prefix={'$'}
                            renderText={value => <div> {value} </div>} />
                    </div>

                </div>


                <div className="active-block">
                    <h3 className="title_a">Achieve Below to Upgrade Rank</h3>

                    <p className={`${+profileData?.pgv >= 50 ? 'check' : 'not-check'}`}>{`PQV >= 50`}</p>
                    <p className={`${+profileData?.pqv >= 750 ? 'check' : 'not-check'}`}>{`PGV >= 750`}</p>
                    <p className={`${profileData?.user_data?.userdetails[0]?.is_autoship_user === "True" ? 'check' : 'not-check'}`}>Autoship Active</p>
                    <p className={`${profileData?.user_data?.userdetails[0]?.membership === true ? 'check' : 'not-check'}`}>Membership</p>

                </div>

            </div>
            <div className="DownlineStatic">
                <h3 className="title_1">Downline Statistics</h3>

                <div className="filleddata">
                    <div className="total-main-now">
                        <div className="data-sts">Total </div>
                        <div className="data-sts"> {profileData?.total_count} </div>
                    </div>
                    <div className="total-main-now">
                        <div className="data-sts">Level1 </div>
                        <div className="data-sts"> {profileData?.level1_count} </div>
                    </div>
                    <div className="total-main-now">
                        <div className="data-sts">Level2 </div>
                        <div className="data-sts"> {profileData?.level2_count} </div>
                    </div>
                    <div className="total-main-now">
                        <div className="data-sts">Level3 </div>
                        <div className="data-sts"> {profileData?.level3_count} </div>
                    </div>
                    <div className="total-main-now">
                        <div className="data-sts">Level4 </div>
                        <div className="data-sts"> {profileData?.level4_count} </div>
                    </div>
                </div>
            </div>
            <div className="newsdata DownlineStatic">
                <h3 className="title_1">News fields</h3>

                {newsdata &&
                    newsdata?.data?.map((news,index) => {
                        return (
                            <div className="newdata" key={index}><strong>{news?.news_title}</strong>  {news?.news_content}</div>
                        )
                    })
                }
            </div>

            <div className="newsdata DownlineStatic">
                <h3 className="title_1">Documents</h3>

                {documentdata?.data?.map((docs) => {
                    return (
                        <div className="newdata" key={docs.document_name}>

                            <Link className="nav-link" exact href={`/${router?.query?.page}/user/documents`}>
                                <a><strong>{docs.document_name}</strong></a>
                            </Link>

                        </div>
                    )
                })
                }
            </div>
        </div>
    )
}
export default Leftsection;