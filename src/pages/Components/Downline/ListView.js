import { FaUser, FaEnvelopeOpen, FaPhoneAlt, FaCalendarCheck, FaTags, FaStore } from "react-icons/fa";
import { useEffect, useState } from 'react';
import NumberFormat from "react-number-format";
// import Moment from "react-moment";
import moment from "moment"
import { index } from "d3-array";
import { BsArrowsExpand, BsFillPeopleFill } from "react-icons/bs";
import { HiDotsHorizontal } from "react-icons/hi";

const TreeViewCustom = ({ state, setState }) => {
    const [level2, setLevel2] = useState()
    const [level3, setLevel3] = useState()
    const [level4, setLevel4] = useState()





    let count_dats = []

    const convert = (names) => {
        const res = {};
        names?.forEach((obj) => {
            const key = `${obj.parent}`;
            if (!res[key]) {
                res[key] = { ...obj, count: 0 };
            };
            res[key].count += 1;
        });
        return Object.values(res);
    };


    for (let i = 2; i <= 4; i++) {
        const names = state && state?.level2 && state[`level${i}`]

        let result = convert(names)
        result.map((filterdata) => {
            count_dats.push({ "parant_id": filterdata.parent, "count": filterdata.count })

        })
    }

    const ShowNextLevel = (id, level) => {

        if (level === 1) {
            setLevel2()
            setLevel3()
            setLevel4()
        }
        if (level === 2) {
            setLevel3()
            setLevel4()
        }
        if (level === 3) {

            setLevel4()
        }




        let setdata = true;
        if (+level === 1) {
            if (level2?.length > 0) {
                const objIndex = level2?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel2()
                }

            }

        }
        if (+level === 2) {
            if (level3?.length > 0) {
                const objIndex = level3?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel3()
                }
            }

        }
        if (+level === 3) {
            if (level4?.length > 0) {
                const objIndex = level4?.findIndex((obj => Number(obj.parent) == Number(id)));
                if (objIndex >= 0) {
                    setdata = false
                    setLevel4()
                }
            }

        }

        let nextlevelData = [];

        {
            setdata === true &&

                state[`level${level + 1}`]?.map((downlineList, index) => {
                    if (+id === +downlineList?.parent) {
                        nextlevelData.push(downlineList)


                        if (+level === 1)
                            setLevel2(nextlevelData)
                        if (+level === 2)
                            setLevel3(nextlevelData)
                        if (+level === 3)
                            setLevel4(nextlevelData)
                    }
                })

        }

    }

    const listviewdata = (downlineList, level) => {

        //  const date = Moment(downlineList?.user?.date_joined).format(`${process.env.Date_Format}`);
        const objIndex = count_dats?.findIndex((obj => Number(obj.parant_id) == Number(downlineList?.id)));
        console.log('downlineList', downlineList);
        return (
            <div className="row"
                onClick={() => { ShowNextLevel(downlineList?.id, level) }}
            >
                <div className="col-md-6 istviewsection">
                    <div className="box">
                        <div className="groupicon-dd">

                            <div class="box-inner">


                                <div className="defaultshow-down">
                                    <div class="icon">
                                        {downlineList?.user?.user_profile?.image ?
                                            <img
                                                src={`${process.env.DOC_URL}/media/${downlineList?.user?.user_profile?.image}`} alt="" width="100" />
                                            :
                                            <FaUser />

                                        }
                                    </div>
                                    <div class="icon-name">
                                        <span className="name_d0">{downlineList?.user?.first_name} {downlineList?.user?.last_name}</span>
                                    </div>
                                    <div class="icon-count">
                                        <HiDotsHorizontal />
                                        <span className="name_d0"><BsFillPeopleFill onClick={(e) => { ShowHideBox(e) }} /> <span className="counts_user-d">{count_dats[objIndex]?.count ? count_dats[objIndex]?.count : 0}</span></span>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>



                    <div className="details-list-section  hide">
                        <div className="hovershow">
                            <div className="user_rank">
                                <h3>{downlineList?.user?.user_rank}</h3>
                            </div>
                            <div className="icons">

                                {downlineList?.user?.show_email === "True" && <a href={`mailto:${downlineList?.user?.email}`}><FaEnvelopeOpen /></a>}
                                {downlineList?.user?.show_phone_no === "True" && <a href={`tel:${downlineList?.user?.public_phone_no}`}><FaPhoneAlt /></a>}

                            </div>
                        </div>

                        <div className="data"><span className="date_title"><FaCalendarCheck />Date Joined : </span><span className="date_data"><strong>
                            {moment(downlineList?.user?.date_joined).format(`${process.env.Date_Format}`)}

                        </strong></span></div>
                        <div className="data"><span className="total_order_title"><FaStore />Total Order :</span> <span className="total_order_data">
                            <strong>{downlineList?.user?.orders_count}</strong>
                        </span></div>
                        <div className="data"><span className="sale_title"><FaTags />Total Sales :</span><span className="sale_data">
                            <strong>
                                {+downlineList?.user?.total_sales > 0 ?
                                    <NumberFormat
                                        value={parseFloat(+downlineList?.user?.total_sales).toFixed(2)}
                                        displayType={'text'} thousandSeparator={true} prefix={'$'}
                                        renderText={value => <div> {value} </div>} />
                                    :
                                    <NumberFormat
                                        value={parseFloat(0).toFixed(2)}
                                        displayType={'text'} thousandSeparator={true} prefix={'$'}
                                        renderText={value => <div> {value} </div>} />
                                }

                            </strong>
                        </span>
                        </div>

                    </div>
                </div>
            </div>

        )
    }

    const ShowHideBox = (e) => {
        e.stopPropagation();

        let node = e.currentTarget?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.querySelectorAll('.details-list-section');
        let data_var = node['0']?.className.split(' ')
        if (data_var.length === 1) {
            e.currentTarget?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.querySelector('.details-list-section')?.classList?.add("hide")

        }
        else {
            e.currentTarget?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.parentNode?.querySelector('.details-list-section')?.classList?.remove("hide")

        }

    }
    return (
        <>
            <div className="row">
                <div className="col-md-3">
                    <div className="Level">
                        <h2>Level1</h2>
                        {state &&
                            state['level1']?.map((downlineList, index) => {
                                return (listviewdata(downlineList, 1))
                            })
                        }
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="Level">
                        <h2>Level2</h2>
                        <div className={`animations ${level2 === undefined ? 'hidden' : ''}`}>

                            {level2 &&

                                level2?.map((downlineList, index) => {
                                    return (listviewdata(downlineList, 2))
                                })
                            }


                        </div>

                    </div>
                </div>
                <div className="col-md-3">
                    <div className="Level">
                        <h2>Level3</h2>
                        <div className={`animations ${level3 === undefined ? 'hidden' : ''}`}>
                            {level3 &&
                                level3?.map((downlineList, index) => {
                                    return (listviewdata(downlineList, 3))
                                })}


                        </div>
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="Level">
                        <h2>Level4</h2>
                        <div className={`animations ${level4 === undefined ? 'hidden' : ''}`}>

                            {level4 &&

                                level4?.map((downlineList, index) => {
                                    return (listviewdata(downlineList, 4))
                                })


                            }
                        </div>

                    </div>
                </div>
            </div>
        </>
    )

}
export default TreeViewCustom;