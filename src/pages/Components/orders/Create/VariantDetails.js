import NumberFormat from 'react-number-format';
import { FaTrash } from "react-icons/fa";

const VariantDetails = ({ productdata, Add, Sub, ProductAutoship, DeleteProduct }) => {

    return (
        <>
            <div className="col-md-4">
                <div className="cart-product-details">
                    {productdata?.Product_name} X {productdata?.quantity} {productdata?.is_autoship}
                </div>
            </div>
            <div className="col-md-2 qty-sec">
                <div className="main-qty-sec">
                    <div className="box">
                        <div id="qty">
                            <button type="button"
                                data-qty={productdata?.quantity}
                                data-product_id={productdata?.product_id}
                                data-variant_id={+productdata?.variant_id}
                                className="sub"
                                onClick={(e) => { Sub(e) }}>-</button>
                            <input
                                name={`${productdata?.variant_id},${null}`}
                                type="text"
                                value={productdata?.quantity}
                                readOnly
                            />
                            <button type="button"
                                data-qty={productdata?.quantity}
                                data-product_id={productdata?.product_id}
                                data-variant_id={+productdata?.variant_id}
                                data-max_qty={+productdata?.total_quantity}
                                className="add"
                                className="add" value={productdata?.quantity} onClick={(e) => { Add(e) }}>+</button>
                        </div>
                    </div>
                    {/* <div className="delete">
                    <FaTrash onClick={()=>{deleteautoship(datas?.product?.id, +datas?.variant?.id)}} />
                </div> */}
                </div>
            </div>

            <div className="col-md-2">
                <div className="cart-product-details">
                    <NumberFormat value={parseFloat(productdata?.is_autoship === false ? +productdata?.cost * +productdata?.quantity : +productdata?.autoShipCost * +productdata?.quantity).toFixed(2)} displayType={'text'} thousandSeparator={true} prefix={'$'} renderText={value => <div>{value}</div>} />

                </div>
            </div>
            <div className="is_autoship col-md-2">
                <div className="cart-product-details">
                    Autoship <input type="checkbox" name="chk" onChange={(e) => {
                        ProductAutoship(e, productdata?.product_id, +productdata?.variant_id)
                    }}></input>
                </div>


            </div>
            <div className="is_autoship col-md-2">
                <div className="cart-product-details">


                    <FaTrash
                        onClick={(e) => {
                            DeleteProduct(e, productdata?.product_id, +productdata?.variant_id)
                        }}
                    />
                </div>


            </div>
        </>
    )
}
export default VariantDetails;