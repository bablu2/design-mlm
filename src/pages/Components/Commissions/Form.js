import * as React from 'react';
import TextField from '@material-ui/core/TextField';
import DateRangePicker from '@material-ui/lab/DateRangePicker';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import Box from '@material-ui/core/Box';
import MobileDateRangePicker from '@material-ui/lab/MobileDateRangePicker';
import Moment from 'moment';
import { useState } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import moment from "moment"

const Form = ({ allcommissiondata, register, IsCustomdate, handleSubmit, onSubmit, setValue, iscustomdate, value, value1, value2, onChange, onChange1, setShowCalender, showcalender }) => {

    const [fromdateshow, ShowfromDate] = useState(false);
    const [tilldateshow, ShowtillDate] = useState(false);
    const wrapperRef1 = React.useRef(null);

    var check = Moment(new Date(), 'YYYY/MM/DD');

    var month = check.format('M');
    var day = check.format('D');
    var year = check.format('YYYY');
    React.useEffect(() => {


        document.addEventListener("click", handleClickOutside, false);
        return () => {
            document.removeEventListener("click", handleClickOutside, false);
        };

    }, []);
    const handleClickOutside = event => {
        if (wrapperRef1.current && !wrapperRef1.current.contains(event.target)) {
            ShowfromDate(false)
            ShowtillDate(false)
            setShowCalender(!showcalender)
        }
    };


    let oldDate = ''
    let test = [{ "name": "abc", "value": '12' }, { "name": "abc1", "value": '1212' }, { "name": "abc12", "value": '121' }]


    let tabledata = []
    allcommissiondata?.map((trans, index) => {

        tabledata.push({

            "Date": moment(trans?.created_at).format('MM/DD/YYYY'),

        })


        {
            test?.map((testdata) => {
                oldDate = moment(trans?.created_at).format('MM/DD/YYYY')

            })
        }
    })

    let array = tabledata
    let result = Object.values(array.reduce((a, { Date }) => {
        a[Date] = (a[Date] || { Date });
        return a;
    }, {}));










    return (
        <>

            <h3 className="title">Commissions</h3>
            <form className="signupform main-sign-frm" onSubmit={handleSubmit && handleSubmit(onSubmit && onSubmit)}>
                <div className="Custom_filter">
                    <div className="date_range_filter">
                        Date Range: <input type="radio"
                            ref={register && register({
                                required: true,
                            })}
                            onClick={() => {

                                IsCustomdate('week')
                            }}
                            checked={iscustomdate === 'week' ? true : false}

                            name="filter_by_date" value="week" /> This Week

                                      <input
                            ref={register && register({
                                required: true,
                            })}
                            onClick={() => {
                                IsCustomdate('month')
                            }}
                            checked={iscustomdate === 'month' ? true : false}

                            type="radio" name="filter_by_date" value="month" /> This Month
                                      <input
                            ref={register && register({
                                required: true,
                            })}
                            checked={iscustomdate === 'custom' ? true : false}

                            type="radio" name="filter_by_date" value="custom"
                            onClick={() => {
                                IsCustomdate('custom')
                            }}
                        /> Custom Date
                            </div>
                    {iscustomdate === 'custom' && <>

                        <div className="date_range_filter" ref={wrapperRef1}>
                            <div className="custom-date-collection">
                                <TextField required id="standard-required" label="From"
                                    value={value1 && Moment(value1).format(`${process.env.Date_Format}`)}
                                    defaultValue={value1 && Moment(value1).format(`${process.env.Date_Format}`)}
                                    className="start"
                                    onClick={() => {
                                        setShowCalender(true)

                                        // let data_date = value1
                                        // onChange()
                                        // onChange(data_date)


                                        ShowfromDate(true)
                                        ShowtillDate(false)

                                    }}

                                />
                        To
                        <TextField id="standard-required" label="End"
                                    //  defaultValue="Hello World"
                                    name="til_date"

                                    className="end"

                                    value={value2 && Moment(value2).format(`${process.env.Date_Format}`)}

                                    onClick={() => {
                                        setShowCalender(true)

                                        ShowtillDate(true)
                                        ShowfromDate(false)


                                    }}
                                />
                            </div>



                            {
                                fromdateshow === true &&
                                <div className="fromdateshow">
                                    <Calendar

                                        onChange={(data) => {
                                            onChange(data)
                                            ShowfromDate(false)
                                            setShowCalender(!showcalender)

                                        }}
                                        value={value1}

                                        tileClassName={({ date, view }) => {
                                            var check = Moment(date, 'MM/DD/YYYY');
                                            let curentdates = moment(date).format('MM/DD/YYYY')
                                            const objIndex = result.findIndex((obj => obj.Date === curentdates));
                                            if (objIndex >= 0) {
                                                return 'highlight'
                                            }


                                        }}


                                        maxDate={new Date()}

                                        tileContent={({ activeStartDate, date, view }) => {


                                            let curentdates = moment(date).format('MM/DD/YYYY')
                                            const objIndex = result.findIndex((obj => obj.Date === curentdates));
                                            if (objIndex >= 0) {
                                                return <div className="tooltip1">
                                                    <span className="tooltiptext">Commission Calculated</span>
                                                </div>
                                            }


                                        }

                                        }

                                    />
                                </div>
                            }
                            {tilldateshow == true &&
                                <div className="tildate">
                                    <Calendar
                                        onChange={(data) => {
                                            onChange1(data)
                                            ShowtillDate(false)
                                            setShowCalender(!showcalender)

                                        }}
                                        value={value2}
                                        tileClassName={({ date, view }) => {
                                            let curentdates = moment(date).format('MM/DD/YYYY')
                                            const objIndex = result.findIndex((obj => obj.Date === curentdates));
                                            if (objIndex >= 0) {
                                                return 'highlight'
                                            }




                                        }}
                                        maxDate={new Date()}

                                        minDate={value1}

                                        tileContent={({ activeStartDate, date, view }) => {
                                            let curentdates = moment(date).format('MM/DD/YYYY')
                                            const objIndex = result.findIndex((obj => obj.Date === curentdates));
                                            if (objIndex >= 0) {
                                                return <div className="tooltip1">
                                                    <span className="tooltiptext">Commission Calculated</span>
                                                </div>
                                            }


                                        }

                                        }
                                    />
                                </div>
                            }
                        </div>
                    </>
                    }


                    <div className="order_id_filter">
                        Order Id :<input type="text" name="order_id"
                            ref={register && register({
                                required: false,
                            })}
                        />
                    </div>
                    <div className="Status">
                        <span className="status-head-1">Status :</span>
                        <select
                            ref={register && register({
                                required: false,
                            })}
                            name="status"
                            className="form-select form-select-lg mb-3"
                            aria-label=".form-select-lg example"
                        >
                            <option value="all" >All</option>

                            <option value="pending" >Pending</option>
                            <option value="paid" >Paid</option>
                        </select>
                    </div>
                </div>


                <div className="get_commision">
                    <button type="submit">Get Commissions</button>

                </div>
            </form>
        </>
    )
}

export default Form;