import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { useRouter } from "next/router";
import Link from "next/link";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function Popups(props) {
  const [open, setOpen] = React.useState(true);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const router = useRouter();
  return (
    <div>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          <h5 className=" modal-title" id="exampleModalLabel">WishList</h5>
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            {props?.message}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          {/* <Button onClick={handleClose} color="primary">
            Disagree
          </Button>
          <Button onClick={handleClose} color="primary">
            Agree
          </Button> */}
          <Button
            color="secondary"
            type="button"
            onClick={() => handleClose()}
          >
            Close
          </Button>
          <Link className="nav-link" exact href={`/${router?.query?.page}/wishlist/wishlist`}>
            <a><Button color="primary" type="button">View wishlist</Button></a>
          </Link>
        </DialogActions>
      </Dialog>
    </div>
  );
}




// import Link from "next/link";
// import { useRouter } from "next/router";
// import React from "react";
// // reactstrap components
// import { Button, Modal, ModalBody, ModalFooter } from "reactstrap";
// function Popups(props) {
//   const [modalOpen, setModalOpen] = React.useState(true);
//   const router = useRouter();
//   return (
//     <>
//       <Modal toggle={() => setModalOpen(!modalOpen)} isOpen={modalOpen}>
//         <div className=" modal-header">
//           <h5 className=" modal-title" id="exampleModalLabel">WishList</h5>
//           <button
//             aria-label="Close"
//             className=" close"
//             type="button"
//             onClick={() => setModalOpen(!modalOpen)}
//           >
//             <span aria-hidden={true}>×</span>
//           </button>
//         </div>
//         <ModalBody>{props?.message}</ModalBody>
//         <ModalFooter>
//           <Button
//             color="secondary"
//             type="button"
//             onClick={() => setModalOpen(!modalOpen)}
//           >
//             Close
//           </Button>
//           <Link className="nav-link" exact href={`/${router?.query?.page}/wishlist/wishlist`}>
//             <a><Button color="primary" type="button">
//               View wishlist
//           </Button></a>
//           </Link>
//         </ModalFooter>
//       </Modal>
//     </>
//   );
// }

// export default Popups;