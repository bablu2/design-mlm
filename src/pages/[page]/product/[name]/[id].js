
import Head from 'next/head'
import Layout from '../../../../Components/Layout'
import { useRouter } from 'next/router'
import { useEffect, useRef, useState } from 'react';
import api from '../../../../api/Apis'
import Link from 'next/link';
import Popups from '../../../../Components/Popup';
import { toast } from 'react-toastify';
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
//import '../../../../../styles/Home.module.css'
import NumberFormat from 'react-number-format';
import ProductPageDesign from '../productPageStyle';
import AccordionComp from '../../../../Components/Accordion';
import _ from 'lodash';
export default function Product(props) {
  const router = useRouter()
  const { id, name, page } = router.query;
  const [productqty, setQty] = useState(1);
  const [addtocartmessage, setAddToCartMessage] = useState();
  const [product, setproduct] = useState();
  const [operation, setoperation] = useState();
  const [productvariantid, setproductvariantid] = useState();
  const [costtype, setcosttype] = useState()
  const [variantdetals, setvariantdetails] = useState();
  const [Logintoken, setToken] = useState()
  const [silksliderlimit, setsilksliderlimit] = useState();
  const [relatedData, setRelatedData] = useState();
  const [nav1, setNav1] = useState(null)
  const [nav2, setNav2] = useState(null)
  let slider1 = []
  let slider2 = []
  // For add to cart product/variant
  const autoshipRef = useRef("");
  const addToCart = (e) => {
    setoperation('cart');
    let a = autoshipRef?.current.value;
    const formData = {
      product_id: id, variant_id: productvariantid,
      token: Logintoken, quantity: productqty, is_autoship: a === 'AutoShip' ? true : false
    };
    api.addToCart(formData).then(res => {
      setoperation('');
      toast.success(res.data.message, {
        duration: 5
      })
      props?.setshowminicart(Math.floor(Math.random() * 100))

    })
  }
  // for add product to wishlist
  const addToWishList = (e) => {
    setoperation('wishlist');
    const formData = { product_id: id, variant_id: productvariantid, token: Logintoken, quantity: productqty };
    api.addToWishlist(formData).then(res => {
      if (res?.data?.code === 1) {
        props?.setupdatecartdata(true)
        setoperation('showpopup');
      }
      setAddToCartMessage(res?.data?.message)
    })
  }
  // slider-loading
  useEffect(() => {
    setNav1(slider1)
    setNav2(slider2)
    const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : null;
    setToken(token)
    { id ? ProductDataFunction(id) : '' }
  }, [id])

  const ProductDataFunction = async (id) => {
    await api.getProductByProductid(id).then(res => {
      setvariantdetails(res?.data?.products?.variants);
      setproduct(res?.data);
      if (res?.data?.category_data?.id) {
        let catId = res?.data?.category_data?.id;
        getProductCategories(catId);
      }
    })
  }
  const getProductCategories = async (catId = null) => {
    await api.getProductByCategories(catId).then((res) => {
      if (res?.status && res?.data) {
        setRelatedData(res?.data?.products);
      }
    }).catch((error) => {
      toast.error('internal error');
    })
  }

  //Related product Checkboxes
  const [checkedBoxes, setCheckboxs] = useState([]);
  const RelatedProductCheckbox = (values) => {
    if (_.find(checkedBoxes, { product_id: values?.product_id })) {
      const data = _.reject(checkedBoxes, (row) => row?.product_id === values?.product_id);
      setCheckboxs(data);
    } else {
      const varientValue = _.map(_.filter(values?.variantData, (row) => row.variant_id !== null), "variant_id")
      setCheckboxs([...checkedBoxes, {
        product_id: values?.product_id,
        quantity: 1,
        variant_id: varientValue?.length > 0 ? varientValue?.[0] : null,
        is_autoship: false
      }]);
    }
  }

  //Multi product add to cart 

  const multiAddToCart = async () => {
    const formData = { products: [...checkedBoxes] }
    await api.multiProductAddToCart(Logintoken, formData).then((response) => {
      if (response?.status === 200 && response?.data?.message) {
        //console.log(response?.data)
        toast.success(response?.data?.message, {
          duration: 5
        });
        props?.setshowminicart(Math.floor(Math.random() * 100))
      }
    }).catch((error) => {

    })
  }
  // After seleting variant this will be called
  const switchVarient = (e) => {
    if (e.target.value !== 'Select varient') {
      setproductvariantid(e.target.value)
      api.getProductByVariantid(e.target.value).then(res => {
        setproduct(res?.data)
        setsilksliderlimit(Object.keys(res?.data?.products?.product_variant_images).length)
      })
    }
    else {
      api.getProductByProductid(id).then(res => {
        setvariantdetails(res?.data?.products?.variants);
        setproduct(res?.data)
        setproductvariantid()
      })
    }
  }
  const ForRatingStar = (count = 0) => {
    const Rating = [];
    for (let i = 1; i <= count; i++) {
      Rating.push(<i className="fa fa-star" aria-hidden="true" key={i + 1}></i>);
    }
    return Rating;
  }
  return (<>
    <Layout>
      <Head><title>{name}</title></Head>
      <ProductPageDesign>
        {/* first section bannner */}
        <div className="product-section-banner">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
              </div>
            </div>
          </div>
        </div>
        {/* {second section} */}
        <div className="product-section-slider">
          <div className="container">
            <div className="row">
              <div className="col-md-6">
                <div className="product-slider">
                  <div className="thumb-slider">
                    <Slider
                      asNavFor={nav1}
                      ref={slider => (slider2 = slider)}
                      //slidesToShow={silksliderlimit >= 3 ? 3 : 1}
                      slidesToShow={4}
                      slidesToScroll={1}
                      swipeToSlide={true}
                      focusOnSelect={true}
                      vertical={true}
                      arrows={false}
                      // verticalSwiping={true}
                      infinite={true}

                    >
                      {product?.products?.product_images?.map((productDataimage, index) => (
                        <img src={`${process.env.API_URL}/${productDataimage['image']}`} key={index + 1} />
                      ))}
                      {productvariantid !== undefined &&
                        product?.products?.product_variant_images?.map((productDataimage, index) => (
                          <img src={`${process.env.API_URL}/${productDataimage['image']}`} key={index + 1} />
                        ))}
                    </Slider>
                  </div>
                  <div className="product-slider-now">
                    <Slider asNavFor={nav2} ref={slider => (slider1 = slider)} arrows={false}>
                      {product?.products?.product_images?.map((productDataimage, index) => (
                        <img src={`${process.env.API_URL}/${productDataimage['image']}`} key={index + 1} />
                      ))}
                      {productvariantid !== undefined &&
                        product?.products?.product_variant_images?.map((productDataimage, index) => (
                          <img src={`${process.env.API_URL}/${productDataimage['image']}`} key={index + 1} />
                        ))}
                    </Slider>
                  </div>


                </div>



                <div className="study-now">
                  <h6>Study Shows Pycnogenol@ Naturally Improves Heart Function</h6>
                  <p>Research reveals Pycnogenol@ and CoQ10 taken as an adjunct to medication improves heart health:blood volume output,physical fitness,blood pressure,as well as heart and respiratory rate</p>
                  <div className="lean-main"><a href="#" className="btn btn-learn">Lean More</a></div>
                </div>
              </div>
              <div className="col-md-5">
                <div className="product-slider-text">
                  <h2 className="title">{product?.products?.name &&
                    `${product?.product_name || ""} ${product?.products?.name || ""}`}
                  </h2>
                  <h6 className="dollar-text">
                    {product?.products?.name && product?.products?.has_variants !== 'True' &&
                      <>
                        {costtype === 'AutoShip' ?
                          <div className="price">
                            <b>Our Prices {page.toUpperCase()}</b>
                            <NumberFormat value={parseFloat(product?.products.autoship_cost_price).toFixed(2)}
                              displayType={'text'}
                              thousandSeparator={true}
                              prefix={'$'}
                              renderText={value => <div>{value}</div>} /></div>
                          :
                          <div className="title product-page-peice">
                            <b>Our Prices {page.toUpperCase()}</b>
                            <NumberFormat value={parseFloat(product?.is_autoship_user === "True" ?
                              product?.products.autoship_cost_price
                              :
                              product?.products.cost_price).toFixed(2)}
                              displayType={'text'}
                              thousandSeparator={true}
                              prefix={'$'}
                              renderText={value => <div>{value}</div>} /></div>
                        }
                      </>
                    }
                  </h6>
                  <h5>Sale Price : US $59.95</h5>
                  <h5>BV : {costtype === 'AutoShip' ? product?.products?.autoship_bonus_value.split()[0] : product?.products?.bonus_value} </h5>
                  <div className="quality">
                    {product?.products?.name && product?.products?.has_variants !== 'True' &&
                      <div className="main-qty-sec">
                        <div className="box">
                          <div className="select">
                            <select className="form-select form-select-lg mb-3" ref={autoshipRef} aria-label=".form-select-lg example" onChange={(e) => { setcosttype(e.target.value) }}>
                              <option value="Normal" >Single</option>
                              <option value="AutoShip" >AutoShip</option>
                            </select>
                          </div>
                        </div>
                        <div className="box">
                          <span>Quantity:</span>
                          <div id="qty">
                            <button type="button" id="sub" className="sub" onClick={(e) => {
                              setQty(productqty > 1 ? productqty - 1 : productqty)
                            }}>-</button>
                            <input type="text" value={productqty} name="qty" onChange={(e) => {
                              setQty(e.target.value)
                            }} min="1" max={product?.products?.quantity} readOnly />
                            <button type="button" id="add" className="add" onClick={(e) => { setQty(productqty + 1) }}>+</button>
                          </div>
                        </div>
                      </div>
                    }
                  </div>
                  <div className="supplement-fact">
                    <a href="#" className="btn btn-supplement">Supplement-facts</a>
                  </div>
                  <div className="review-section-now">
                    <ul>
                      <li>{ForRatingStar(5)}</li>
                      <li><a href="#">113 reviews</a></li>
                      <li><a href="#">Write a review</a></li>
                    </ul>
                  </div>
                  <div className="cart-add">
                    {operation === 'showpopup' && <Popups message={addtocartmessage} />}
                    {productvariantid || variantdetals?.length <= 0 ?
                      <>
                        {productvariantid !== undefined &&
                          <div className="select-variant-dropdown">
                            <p>Select Variant</p>
                            <div className="select">
                              <select className="form-select form-select-lg mb-3" aria-label=".form-select-lg example" onChange={(e) => { switchVarient(e) }}>
                                <option value="Select varient">Select variant</option>
                                {variantdetals?.map((productvariants, index) => {
                                  return (<>
                                    <option key={index + 1} value={productvariants?.id}
                                      selected={productvariantid === productvariants?.id}>{productvariants?.name}</option>
                                  </>)
                                })
                                }
                              </select>
                            </div>
                          </div>
                        }
                        <div className={`btn-sec Add_to_cart ${product?.products?.is_stock_available === 'True' ? '' : 'out-of-stock'}`} >
                          <div className="Add_to_cart">
                            <button disabled={product?.products?.is_stock_available === 'True' ? false : true}
                              type="button" id={product?.products?.id}
                              onClick={(e) => addToCart(e)}>
                              {product?.products?.is_stock_available === 'True' ? 'Add To Cart' : 'Sold Out'}
                            </button>
                          </div>
                        </div>
                        {props.isLogin === true &&
                          <div className='btn-sec' >
                            <div className="Add_to_wishList">
                              <button type="button" id={product?.products?.id} onClick={(e) => addToWishList(e)}>Add To WishList</button>
                            </div>
                          </div>}
                      </>
                      :
                      <div className="select-variant-dropdown">
                        <p>Select Variant</p>
                        <div className="select">
                          <select className="form-select form-select-lg mb-3" aria-label=".form-select-lg example" onChange={(e) => {
                            switchVarient(e)
                          }}>
                            <option value="Select varient">Select Variant</option>
                            {variantdetals?.map((productvariants, index) => {
                              return (
                                <option key={index + 1} value={productvariants?.id}
                                  selected={productvariantid === productvariants?.id}>
                                  {productvariants?.name}
                                </option>)
                            })}
                          </select>
                        </div>
                      </div>
                    }
                  </div>
                  <div className="accordin">
                    {(() => {
                      const data = [
                        { title: "Description", content: product?.products?.description },
                        { title: "Ingredients", content: product?.products?.ingredients },
                        { title: "Suggested Use", content: product?.products?.suggested_use },
                        { title: "Returns", content: product?.products?.return_product }
                      ];
                      return data?.map((value, index) => (
                        <div key={index + 1} className={`accordion` + index}>
                          <AccordionComp title={value.title} content={value.content} />
                        </div>
                      ))
                    })()}
                  </div>
                  <div className="frequestion-section">
                    <h3>Frequently Brought Together</h3>
                    <div className="product-checkout">
                      <ul>
                        {relatedData?.map((value, index) => {
                          let names = value?.name.replaceAll('25%', '')
                          let catname1 = names.replaceAll('\t', '')
                          let catname = catname1.replaceAll(' ', '_')
                          if (id !== value?.id) {
                            return (
                              <li key={index + 1} >
                                <div className="product-firxt">
                                  <div className="product_1">
                                    <Link href={`/${page}/product/${catname}/${value?.id}`}>
                                      <a className="thumbnail">
                                        <img src={`${process.env.API_URL}/${value?.product_images[0]?.['image']}`} />
                                        <h5>{value?.name}</h5>
                                      </a>
                                    </Link>
                                    <h6>
                                      <NumberFormat value={parseFloat(product?.products?.cost_price).toFixed(2)}
                                        displayType={'text'}
                                        thousandSeparator={true}
                                        prefix={'$'}
                                        renderText={value => <div>{value}</div>} />
                                    </h6>
                                    <div className="form-group">
                                      <input type="checkbox"
                                        id="html"
                                        name={"checkbox" + index}
                                        value={value?.id}
                                        defaultChecked={_.find(checkedBoxes, { product_id: value?.id })}
                                        onChange={() => RelatedProductCheckbox({
                                          product_id: value?.id,
                                          quantity: 1,
                                          variantData: value?.product_images,
                                          is_autoship: false
                                        })} />
                                    </div>
                                  </div>
                                </div>
                              </li>)
                          }
                        })}
                      </ul>
                    </div>
                    <div className="total-text">
                      <div className="total_selected">
                        <h4>Total Selected:</h4>
                        <div className="zero">{checkedBoxes?.length > 0 ? +(checkedBoxes?.length) : 0}</div>
                      </div>
                      <div className="total-cart">
                        <button className="btn btn-cart" onClick={() => multiAddToCart()}>Add to Cart</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* {third section} */}
        <div className="health_supplement_section">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <h3>Kaire has set a standard in health supplements.Live Better,Longer!</h3>
                <p>Join us on a path to a better you.Lose weight,transform your health,and live the life you have always dreamed of*.Pioneering the Antioxidant Revolution with our legendary Martime Prime Pycnogenol@,Kaire has set a standard in health supplements.Live Better,Longer!Join us on a path to a better you.Lose weight,transform your health,and live the life you have always dreamed of*.Pioneering the Antioxidant Revolution with our legendary Maritime Prime Pycnogenol@,Kaire has set a standard in heath supplements.Live Better,Longer!</p>
              </div>
            </div>
            <div className="row both-text">
              <div className="col-md-6">
                <div className="karie-pic-now">
                  <img src="/images/t1.png" />
                </div>
                <div className="box-text">
                  <div className="box-left">
                    <h4>Our Unique Delivery System ofPlant-based Enzymes make Maritime Prime <span className="green-text">4X More Effective </span>Than'store-bought'Brands</h4>
                  </div>
                  <div className="box-right">
                    <img src="/images/z1.png" />
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="karie-pic-now">
                  <img src="/images/t2.png" />
                </div>
                <div className="lisy-ul box-text">
                  <ul>
                    <li>Lower Blood Pressure</li>
                    <li>Reduce Cholesterol Levels</li>
                    <li>Improve Cardiovascular(Heart)Health</li>
                    <li>Improve Cognitive Functions-Memory & ADHD </li>
                    <li>Promote Healthier Skin-Reduce Fine Lines,Wrinkles & Brown Skin Sports</li>
                  </ul>
                  <ul>
                    <li>Reduce Adverse Effects of Diabetes</li>
                    <li>Improve joint Mobility & Flexibility</li>
                    <li>Help Menstrual Disorders-PMS&Endometriosis</li>
                    <li>Help Asthma & Havy Fever</li>
                    <li>Improve & Increase Sports Performance</li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="row both-text">
              <div className="col-md-6">
                <div className="image_secton_">
                  <div className="karie-pic-now">
                    <h3>365-day Guarantee</h3>
                  </div>
                  <div className="box-text2">
                    <h5>Maritime Prime 180 Works !As a matter of fact we are so confident that we are offering your a 365-day money back guarantee on your purchase.<br /><span className="green-text">No questions asked </span></h5>

                  </div>
                  <div className="gar-img">
                    <img src="/images/gar-img.png" />
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="blue-bg">
                  <h2>Maritime Prime 180</h2>
                  <h5>Over 3million bottles sold worldwide.<br />Maritime Prime is second to none.</h5>
                  <div className="logo-360"><img src="/images/main-logo.png" /></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* {section 4} */}
        <div className="customer-review-now">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div className="review-customer">
                  <h5>What are people saying about this product</h5>
                  <h2>Customer Reviews</h2>
                  <div className="name-review">
                    <ul>
                      <li><strong>Newest</strong></li>
                      <li>Popular</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            {(() => {
              return [
                {
                  name: 'Donna',
                  content: `My husband and I were in this business when it was first getting off the ground!Glad to see it's still going strong!
                  We need Pycnogonal ! Regrettably we should have stayed on it all these years!`
                },
                {
                  name: 'Jimmy Jinetas',
                  content: `I'm starting with Pycnogenol again now due to medical emergency.20 years ago when I was 45,
                  I took six tabs a day and right away.I felt better. Where my sleeping was marred with siren like dizziness in the morning and my days had a constricted feeling,
                  that all went away in a few days--should have stuck with it`
                },
                {
                  name: 'Marny Minarsch',
                  content: `I have been taking Maritime Prime for 30+ years.It's the only supplement that has ever helped my mood swings!
                  Of course,it helps my whole body,but If I am feeling "down" I increase to 1tab for each 20Ibs, 
                  and within days,I'm Feeling up and happy again! It's the only supplement that I have taken consistenly for this long.
                  I intend to use it for another 30 years!Great company as well.`
                },
                {
                  name: 'Richard Burnette',
                  content: `I have taken Pycnogenol since 1992.I have had and seen incredible results in people who where really sick.
                  This maybe the only product that can really improve your health when nothing else seems to work.
                  I take it because it stays active in your body for 72 hours and that is the key to combatting free radicals.
                  I am glad to share Kaire and help people.I took it for sinus infections and it just stopped it all.
                  Then I noticed I had more flexible joints and was sleeping better.It have found it takes 30 to 90days to really start getting serious results.
                  I personally take 20mg per 20Ibs of body weight.I weigh 200 pounds so I take 10 per day for 30 days then back down to 4 to 5 per day. 
                  This what I do beacause I understand how it works and why it works.Richard Burnette Chattanooga,TN`
                }
              ]?.map((data, index) => (
                <div className="row client-review" key={index + 1}>
                  <div className="col-md-2">
                    <div className="profile-1">
                      <h6>{data?.name}</h6>
                    </div>
                  </div>
                  <div className="col-md-8">
                    <div className="comment">
                      <p>{data?.content}</p>
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="comment-stars">{ForRatingStar(5)}</div>
                  </div>
                </div>
              ))
            })()}
            <div className="row">
              <div className="col-md-12">
                <div className="btn-pre-view">
                  <a href="#" className="prev-btn-now">Previous</a>
                  <a href="#" className="prev-btn-now">Next</a>
                </div>
                <div className="question-text">
                  <div className="stay-touch-part">
                    <h5>Stay in Touch</h5>
                  </div>
                  <div className="form-subscribe">
                    <form className="form-inline">
                      <div className="form-group">
                        <label className="col-form-label">Subscribe</label>
                        <input type="text" readOnly className="form-control-plaintext" id="staticEmail" value="*Your email address " />
                      </div>
                      <button type="submit" className="btn btn-primary">Subscribe</button>
                    </form>
                  </div>
                </div>
                <div className="box-text-bnner">
                  <img src="/images/text-now-bnner.png" />
                  <h4>*Individual results may vary and are not guaranteed</h4>
                </div>
              </div>
            </div>

          </div>
        </div>
      </ProductPageDesign>
    </Layout>
  </>

  )
}

export const getStaticPaths = async ({ id }) => {
  let paths;
  const defaultValues = {
    paths: [],
    fallback: false,
  };

  try {
    // const allSlugs = ["us/product/Test2%20Product/12/", "us/product/Test3%20Product/13/"];

    const res = await fetch(`${process.env.getAllProduct}?slug=us`)
    let data_page = await res?.json();
    data_page = data_page?.products
    paths = data_page?.map((c) => {
      let names = c?.name.replace('25%', '')
      let catname = names.replace('\t', '')
      catname = catname.replace(' ', '_')
      catname = catname.replace(' ', '_')
      catname = catname.replace(' ', '_')
      catname = catname.replace(' ', '_')
      catname = catname.replace(' ', '_')
      return (`/us/product/${catname}/${c?.id}`)
    });    // paths = allSlugs.map((slug) => "/" + slug);

  } catch (e) {
    return defaultValues;
  }

  return {
    paths,
    fallback: false,
  };
};


export const getStaticProps = async ({ params: { page, id } }) => {
  const res = await fetch(`${process.env.getAllProduct}?slug=us`)
  let data_page = await res.json();
  // data_page = data_page//?.products
  return {
    props: {
      id, data_page
    },
    revalidate: 1,
  };
};