import Head from "next/head";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Leftnav from "../../../../Components/Leftnav";
import api from '../../../../api/Apis'
import * as React from 'react';
import TextField from '@material-ui/core/TextField';
import DateRangePicker from '@material-ui/lab/DateRangePicker';
import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import Box from '@material-ui/core/Box';
import MobileDateRangePicker from '@material-ui/lab/MobileDateRangePicker';
import Moment from 'moment';
import ReactPaginate from 'react-paginate';
import Form from "../../../Components/Commissions/Form";
import FormData from "../../../Components/Commissions/FormData";
import GraphicalCommission from "../../../Components/Commissions/GraphicalCommission";
import ViewOrderDetails from "../../../Components/Commissions/ViewOrderDetails";
import ExportCSV from "../../../Components/Commissions/ExportCSV";
import { useRouter } from "next/router";
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
const Commission = ({ validateauth, showprotectedpage }) => {
    const [LoginToken, setLoginToken] = useState()
    const { register, handleSubmit, errors } = useForm();
    const [value, setValue] = useState([null, null]);
    const [iscustomdate, IsCustomdate] = useState('month')
    const [viewtype, setViewtype] = useState('list')
    const [offset, setOffset] = useState(0);
    const [data, setData] = useState();
    const [perPage] = useState(10);
    const [pageCount, setPageCount] = useState(0)
    const [commissionData, setcommissionData] = useState()
    const [commissionDataError, setcommissionDataError] = useState()
    const [filterdata, setfilterdata] = useState()
    const [showorderdetails, setshowdetailsorder] = useState(false)
    const [orderid, setorderid] = useState()
    const [allcommissiondata, setAllCommisiondata] = useState()
    const [firstdate, setFirstDate] = useState()
    const [lastdate, setLastDate] = useState()
    const router = useRouter()

    const [value1, onChange] = useState(new Date());
    const [value2, onChange1] = useState(new Date());
    const [showcalender, setShowCalender] = useState();

    useEffect(() => {
        validateauth()

        // if (Object.keys(router?.query).length > 1) {
        //     router.push('/us/404/')
        // }

        if (!router?.query?.Commission_order_id) {
            if (Object.keys(router?.query).length > 1) {
                router.push('/us/404/')
            }
        }
        const token = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
        let dataforfilter = []
        dataforfilter.order_id = router?.query?.Commission_order_id;
        dataforfilter.filter_by_date = iscustomdate;


        setLoginToken(token)
        {
            filterdata === undefined &&
                router?.query?.Commission_order_id ?
                api.getCommissionsFilter(dataforfilter, token).then(res => {
                    setAllCommisiondata(res?.data?.commissions_data)

                    const slice = res?.data?.commissions_data?.slice(offset, offset + perPage)
                    setPageCount(Math.ceil(res?.data?.commissions_data?.length / perPage))
                    setcommissionData(slice)
                    if (res?.data?.code === 1) {
                        setcommissionDataError(res?.data?.message)
                    }
                    if (res?.data?.code === 0) {
                        setcommissionDataError()
                    }
                })

                :

                api.getCommissionReport(token, 'month').then(res => {
                    setAllCommisiondata(res?.data?.commissions_data)

                    const slice = res?.data?.commissions_data?.slice(offset, offset + perPage)
                    setPageCount(Math.ceil(res?.data?.commissions_data?.length / perPage))
                    setcommissionData(slice)
                    if (res?.data?.code === 1) {
                        setcommissionDataError(res?.data?.message)
                    }
                    // setcommissionData(res?.data)

                })
        }
    }, [offset, router?.query?.Commission_order_id]);

    const onSubmit = data => {
        setfilterdata(data)
        if (iscustomdate === 'custom') {
            // let daterange = convert(value);
            // daterange = daterange.split(" - ");
            // const from_date = Moment(daterange[0]).format('YYYY-MM-DD');
            // const til_date = Moment(daterange[1]).format('YYYY-MM-DD');
            data.from_date = Moment(value1).format('YYYY-MM-DD')
            data.til_date = Moment(value2).format('YYYY-MM-DD')
        }
        api.getCommissionsFilter(data, LoginToken).then(res => {
            setAllCommisiondata(res?.data?.commissions_data)

            const slice = res?.data?.commissions_data?.slice(offset, offset + perPage)
            setPageCount(Math.ceil(res?.data?.commissions_data?.length / perPage))
            setcommissionData(slice)
            if (res?.data?.code === 1) {
                setcommissionDataError(res?.data?.message)
            }
            if (res?.data?.code === 0) {
                setcommissionDataError()
            }
        })

    }

    const convert = dateRange =>
        dateRange.map(date => new Intl.DateTimeFormat('en-US').format(new Date(date)))
            .join(" - ")
    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * perPage;
        if (filterdata !== undefined) {
            FilterData(filterdata, offset)
        }
        setOffset(offset)

    };



    const FilterData = (data, offset) => {
        setfilterdata(data)
        if (iscustomdate === 'custom') {
            // let daterange = convert(value);
            // daterange = daterange.split(" - ");
            // const from_date = Moment(daterange[0]).format('YYYY-MM-DD');
            // const til_date = Moment(daterange[1]).format('YYYY-MM-DD');
            // data.from_date = from_date
            // data.til_date = til_date

            data.from_date = Moment(value1).format('YYYY-MM-DD')
            data.til_date = Moment(value2).format('YYYY-MM-DD')
        }
        api.getCommissionsFilter(data, LoginToken).then(res => {
            const slice = res?.data?.commissions_data?.slice(offset, offset + perPage)
            setPageCount(Math.ceil(res?.data?.commissions_data?.length / perPage))
            setcommissionData(slice)
            if (res?.data?.code === 1) {
                setcommissionDataError(res?.data?.message)
            }
            if (res?.data?.code === 0) {
                setcommissionDataError()
            }
        })

    }

    const ShoworderDetails = (id) => {
        setshowdetailsorder(true)
        setorderid(id)
    }

    let CsvData = [];
    const setShowCalenderData = (val) => {
        setShowCalender(val)
    }
    return (<>
        <Head>
            <title>Commissions</title>
        </Head>
        {showprotectedpage === true &&

            <div className="mainorder-detail-sec commision-now ">
                <div className="container">


                    <div className="row">
                        <Leftnav />
                        <div className="col-md-9 dashboard-main">

                            {showorderdetails === true ?
                                <ViewOrderDetails setshowdetailsorder={setshowdetailsorder} orderid={orderid} setorderid={setorderid} />
                                :
                                <>
                                    <Form
                                        allcommissiondata={allcommissiondata}
                                        value1={value1}
                                        value2={value2}
                                        setShowCalender={setShowCalenderData}
                                        showcalender={showcalender}
                                        onChange1={onChange1}
                                        onChange={onChange}
                                        value={value} setValue={setValue} iscustomdate={iscustomdate} register={register} IsCustomdate={IsCustomdate} handleSubmit={handleSubmit} onSubmit={onSubmit} />
                                    {commissionDataError === undefined &&
                                        <div className="export-to-xls">
                                            <ExportCSV csvData={CsvData} fileName={`CommissionReport ${lastdate}  -  ${firstdate}`} />

                                        </div>
                                    }

                                    <div className={`groups tabs ${showcalender === true ? 'commission-tab' : ''}`}>
                                        <ul className="tabs">
                                            <li
                                                className={`col-md-6 ${viewtype === 'list' ? 'active' : ''}`}
                                                onClick={() => {
                                                    setViewtype('list')
                                                }}

                                            >
                                                <button>List View</button></li>
                                            <li
                                                //className="col-md-6"
                                                className={`col-md-6 ${viewtype === 'graphical' ? 'active' : ''}`}

                                                onClick={() => {
                                                    setViewtype('graphical')
                                                }}
                                            ><button>Graphical View</button></li>
                                        </ul>
                                    </div>
                                    {viewtype === "graphical" ?
                                        <div className="graphical-rep-commission">
                                            <GraphicalCommission
                                                iscustomdate={iscustomdate}
                                            />
                                        </div>


                                        :


                                        <div className="comission-data-table">
                                            <FormData setLastDate={setLastDate} setFirstDate={setFirstDate} allcommissiondata={allcommissiondata} CsvData={CsvData} commissionDataError={commissionDataError} handlePageClick={handlePageClick} pageCount={pageCount} commissionData={commissionData} ShoworderDetails={ShoworderDetails} />

                                        </div>

                                    }
                                </>

                            }
                        </div>
                    </div>

                </div>
            </div>
        }
    </>)
}

export default Commission;