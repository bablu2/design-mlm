import React, { useEffect, useRef, useState } from 'react';
import { useRouter } from 'next/router'
import Router from 'next/router';
import NavBar from '../Components/NavBar'
import '../../styles/Home.module.css'
import '../../styles/Responsive.module.css'
import Footer from '../Components/Footer';
import '../../styles/globals.css'
import api from '../api/Apis'
import Minicart from './[page]/cart/Minicart';
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import 'bootstrap/dist/css/bootstrap.min.css'
import { Carousel } from 'react-responsive-carousel';
import cookieCutter from 'cookie-cutter'
import { Provider } from "react-redux";
import store from '../store/index'

function MyApp({ Component, pageProps }) {
  const wrapperRef = useRef(null);
  const [isLogin, setisLogin] = useState();
  const [isLoginToken, setisLoginToken] = useState();
  const [usernotifications, setUsernotification] = useState()

  const [isLogout, setisLogout] = useState();
  const [oldpath, setoldpath] = useState();
  const [counts, setcounts] = useState()
  const [updatecartdata, setupdatecartdata] = useState(false)
  var randomstring = require("randomstring");
  const router = useRouter()
  const [showminicart, setshowminicart] = useState(false)
  const [showloader, setshowloader] = useState(false)
  const [showprotectedpage, setShowProtectedPage] = useState(false)

  const code = router?.query?.token;
  // check current path and valiadte 

  if (router.pathname === '/[page]/login') {
    if (isLogin === true) {
      if (oldpath === '/' || oldpath === '/us/signup/' || oldpath === '/us/login/') {
        router.push(`/`);
      }
      else {
        router.back()
      }
    }
  }
  if (router.pathname === '/[page]/signup') {
    if (isLogin === true) {
      router.back()
    }
  }

  // on click logout
  const handleLogout = e => {
    e.preventDefault();
    api.getAllCartProduct(isLogin).then(res => {
      setcounts(res?.data)

    })
    setisLogout(true);
  }
  useEffect(() => {
    {
      setisLogin(localStorage.getItem('Login') ? JSON.parse(localStorage.getItem('Login')) : false);
      const formdata = localStorage.getItem('Token') ? localStorage.getItem('Token') : '';
      api.getUserNotifications(formdata).then(res => {
        setUsernotification(res?.data?.notifications)
      })
      setisLoginToken(formdata)
      if (formdata === '' || formdata === undefined) {
        if (cookieCutter.get('sessionkey')?.length !== 16) {
          cookieCutter.set('sessionkey', randomstring.generate(16))

        }
      }

      isLogout == true &&
        api.logoutApi(formdata).then(res => {
          localStorage.removeItem('Token');
          localStorage.removeItem('address_id');

          setisLogin(false);
          localStorage.setItem('Login', false);

          setisLogout(false);
          router.push(`/`);
        })

      updatecartdata === true &&
        api.getAllCartProduct(formdata).then(res => {
          setcounts(res?.data)
          setupdatecartdata(false)

        })

      formdata !== '' && isLogin === true &&
        api.LoginCheck(formdata).then(res => {
          if (res?.data?.code === 0) {
            setisLogout(true);


          }

        })

    }


    code !== undefined &&
      api.LoginCheckByurl(code).then(res => {

        if (res?.data?.code === 1) {
          console.log(res)
          setisLogin(true);
          localStorage.setItem('Login', true);
          localStorage.setItem('Token', code);
          router.push(`/us/user/dashboard/`);

        }

      })

  }, [isLogout, updatecartdata, code]);

  Router.onRouteChangeStart = () => {
    setshowloader(true)

  };

  Router.onRouteChangeComplete = () => {
    setshowloader(false)

  };

  Router.onRouteChangeError = () => {
    setshowloader(true)

  };

  const validateauth = () => {


    api.LoginCheck(isLoginToken).then(res => {
      if (res?.data?.code === 0) {
        setisLogout(true);
        setShowProtectedPage(false);


      }
      if (res?.data?.code === 1) {
        setShowProtectedPage(true);


      }

    })
  }
  store.subscribe(() => console.log(store.getState()))
  return (<>
    <Provider store={store}>
      <NavBar usernotifications={usernotifications} setoldpath={setoldpath} handleLogout={handleLogout} isLogin={isLogin} setisLogin={setisLogin} counts={counts} updatecartdata={setupdatecartdata} />
      <Component validateauth={validateauth} setshowloader={setshowloader} setcounts={setcounts} setshowminicart={setshowminicart} setoldpath={setoldpath} oldpath={oldpath} setisLogin={setisLogin} isLogin={isLogin} {...pageProps} setupdatecartdata={setupdatecartdata} setUsernotification={setUsernotification} showprotectedpage={showprotectedpage} />
      <Minicart setcounts={setcounts} showminicart={showminicart} showminicart={showminicart} setshowminicart={setshowminicart} counts={counts} />
      <Footer showloader={showloader} />
    </Provider>
  </>)
}

export default MyApp
